﻿using BaseballApp.Models;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class NewsSlideMap : EntityTypeConfiguration<NewsSlide>
    {
        public NewsSlideMap()
        {
            ToTable("NewsSlide");

            HasKey(t => t.Id);
        }
    }
}