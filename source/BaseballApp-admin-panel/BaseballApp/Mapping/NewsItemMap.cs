﻿using BaseballApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class NewsItemMap : EntityTypeConfiguration<NewsItem>
    {
        public NewsItemMap()
        {
            ToTable("NewsItem");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(t => t.CategoryId)
                .HasColumnName("CategoryId")
                .IsOptional();
            Property(t => t.Title)
                .HasColumnName("Title")
                .HasMaxLength(50)
                .IsRequired();
            Property(t => t.ShortNews)
                .HasColumnName("ShortNews")
                .IsRequired();
            Property(t => t.FullNews)
                .HasColumnName("FullNews")
                .IsRequired();
            Property(t => t.PubDate)
                .HasColumnName("PubDate")
                .IsRequired();
            Property(t => t.Image)
                .HasColumnName("Image")
                .HasMaxLength(50)
                .IsRequired();

            HasOptional(t => t.Category)
                .WithMany(t => t.NewsItems)
                .HasForeignKey(d => d.CategoryId)
                .WillCascadeOnDelete(false);
            HasRequired(t => t.NewsSlide)
                .WithRequiredPrincipal(t => t.NewsItem)
                .WillCascadeOnDelete(true);
        }
    }
}