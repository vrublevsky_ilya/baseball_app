﻿using BaseballApp.Models;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class MatchTeamsMap : EntityTypeConfiguration<MatchTeams>
    {
        public MatchTeamsMap()
        {
            ToTable("MatchTeams");

            HasKey(t => new { t.MatchId, t.TeamId });

            Property(t => t.MatchId)
                .HasColumnName("MatchId")
                .IsRequired();
            Property(t => t.TeamId)
                .HasColumnName("TeamId")
                .IsRequired();
            Property(t => t.Inning1)
                .HasColumnName("Inning1")
                .IsOptional();
            Property(t => t.Inning2)
                .HasColumnName("Inning2")
                .IsOptional();
            Property(t => t.Inning3)
                .HasColumnName("Inning3")
                .IsOptional();
            Property(t => t.Inning4)
                .HasColumnName("Inning4")
                .IsOptional();
            Property(t => t.Inning5)
                .HasColumnName("Inning5")
                .IsOptional();
            Property(t => t.Inning6)
                .HasColumnName("Inning6")
                .IsOptional();
            Property(t => t.Inning7)
                .HasColumnName("Inning7")
                .IsOptional();
            Property(t => t.Inning8)
                .HasColumnName("Inning8")
                .IsOptional();
            Property(t => t.Inning9)
                .HasColumnName("Inning9")
                .IsOptional();
            Property(t => t.R)
                .HasColumnName("R")
                .IsOptional();
            Property(t => t.H)
                .HasColumnName("H")
                .IsOptional();
            Property(t => t.E)
                .HasColumnName("E")
                .IsOptional();

            HasRequired(t => t.Match)
                .WithMany(t => t.MatchTeams)
                .HasForeignKey(d => d.MatchId)
                .WillCascadeOnDelete(false);
            HasRequired(t => t.Team)
                .WithMany(t => t.MatchTeams)
                .HasForeignKey(d => d.TeamId)
                .WillCascadeOnDelete(false);
        }
    }
}