﻿using BaseballApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class PlayerMap : EntityTypeConfiguration<Player>
    {
        public PlayerMap()
        {
            ToTable("Player");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(t => t.CityId)
                .HasColumnName("CityId")
                .IsOptional();
            Property(t => t.PositionId)
                .HasColumnName("PositionId")
                .IsOptional();
            Property(t => t.TeamId)
                .HasColumnName("TeamId")
                .IsOptional();
            Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(50)
                .IsRequired();
            Property(t => t.Surname)
                .HasColumnName("Surname")
                .HasMaxLength(50)
                .IsRequired();
            Property(t => t.Birthday)
                .HasColumnName("Birthday")
                .IsOptional();
            Property(t => t.Number)
                .HasColumnName("Number")
                .IsOptional();
            Property(t => t.Height)
                .HasColumnName("Height")
                .IsOptional();
            Property(t => t.Weight)
                .HasColumnName("Weight")
                .IsOptional();
            Property(t => t.Attack)
                .HasColumnName("Attack")
                .HasMaxLength(1)
                .IsOptional();
            Property(t => t.Protection)
                .HasColumnName("Protection")
                .HasMaxLength(1)
                .IsOptional();
            Property(t => t.MainRoster)
                .HasColumnName("MainRoster")
                .IsRequired();
            Property(t => t.Photo)
                .HasColumnName("Photo")
                .HasMaxLength(50)
                .IsOptional();
            Property(t => t.DebutMajorLeague)
                .HasColumnName("DebutMajorLeague")
                .IsOptional();
            Property(t => t.PlaceOfStudy)
                .HasColumnName("PlaceOfStudy")
                .HasMaxLength(50)
                .IsOptional();

            HasOptional(t => t.City)
                .WithMany(t => t.Players)
                .HasForeignKey(d => d.CityId)
                .WillCascadeOnDelete(false);
            HasOptional(t => t.Position)
                .WithMany(t => t.Players)
                .HasForeignKey(d => d.PositionId)
                .WillCascadeOnDelete(false);
            HasOptional(t => t.Team)
                .WithMany(t => t.Players)
                .HasForeignKey(d => d.TeamId)
                .WillCascadeOnDelete(false);
        }
    }
}