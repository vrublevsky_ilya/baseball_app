﻿using BaseballApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class VideoSlideMap : EntityTypeConfiguration<VideoSlide>
    {
        public VideoSlideMap()
        {
            ToTable("VideoSlide");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(t => t.VideoCategoryId)
                .HasColumnName("VideoCategoryId")
                .IsOptional();
            Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(50)
                .IsRequired();
            Property(t => t.Url)
                .HasColumnName("Url")
                .HasMaxLength(100)
                .IsRequired();
            Property(t => t.Image)
                .HasColumnName("Image")
                .HasMaxLength(50)
                .IsOptional();

            HasOptional(t => t.VideoCategory)
                .WithMany(t => t.VideoSlides)
                .HasForeignKey(d => d.VideoCategoryId)
                .WillCascadeOnDelete(false);
        }
    }
}