﻿using BaseballApp.Models;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class WidgetNextMatchMap : EntityTypeConfiguration<WidgetNextMatch>
    {
        public WidgetNextMatchMap()
        {
            ToTable("WidgetNextMatch");

            HasKey(t => t.Id);
        }
    }
}