﻿using BaseballApp.Models;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class TournamentTeamsMap : EntityTypeConfiguration<TournamentTeams>
    {
        public TournamentTeamsMap()
        {
            ToTable("TournamentTeams");

            HasKey(t => new { t.TournamentId, t.TeamId });

            Property(t => t.TournamentId)
                .HasColumnName("TournamentId")
                .IsRequired();
            Property(t => t.TeamId)
                .HasColumnName("TeamId")
                .IsRequired();
            Property(t => t.Place)
                .HasColumnName("Place")
                .IsOptional();
            Property(t => t.Victories)
                .HasColumnName("Victories")
                .IsRequired();
            Property(t => t.Defeats)
                .HasColumnName("Defeats")
                .IsRequired();

            HasRequired(t => t.Tournament)
                .WithMany(t => t.TournamentTeams)
                .HasForeignKey(d => d.TournamentId)
                .WillCascadeOnDelete(false);
            HasRequired(t => t.Team)
                .WithMany(t => t.TournamentTeams)
                .HasForeignKey(d => d.TeamId)
                .WillCascadeOnDelete(false);
        }
    }
}