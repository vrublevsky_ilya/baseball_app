﻿using BaseballApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class TeamStatsMap : EntityTypeConfiguration<TeamStats>
    {
        public TeamStatsMap()
        {
            ToTable("TeamStats");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(t => t.TeamId)
                .HasColumnName("TeamId")
                .IsOptional();
            Property(t => t.Year)
                .HasColumnName("Year")
                .IsRequired();
            Property(t => t.Victories)
                .HasColumnName("Victories")
                .IsRequired();
            Property(t => t.Defeats)
                .HasColumnName("Defeats")
                .IsRequired();

            HasOptional(t => t.Team)
                .WithMany(t => t.TeamStats)
                .HasForeignKey(d => d.TeamId)
                .WillCascadeOnDelete(false);
        }
    }
}