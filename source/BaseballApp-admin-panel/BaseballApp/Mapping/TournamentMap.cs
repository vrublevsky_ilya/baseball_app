﻿using BaseballApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class TournamentMap : EntityTypeConfiguration<Tournament>
    {
        public TournamentMap()
        {
            ToTable("Tournament");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(t => t.JudgeId)
                .HasColumnName("JudgeId")
                .IsOptional();
            Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(50)
                .IsRequired();
            Property(t => t.Description)
                .HasColumnName("Description")
                .IsOptional();
            Property(t => t.StartDate)
                .HasColumnName("StartDate")
                .IsRequired();
            Property(t => t.EndDate)
                .HasColumnName("EndDate")
                .IsRequired();
            Property(t => t.Logo)
                .HasColumnName("Logo")
                .HasMaxLength(50)
                .IsOptional();

            HasOptional(t => t.Judge)
                .WithMany(t => t.Tournaments)
                .HasForeignKey(d => d.JudgeId)
                .WillCascadeOnDelete(false);
        }
    }
}