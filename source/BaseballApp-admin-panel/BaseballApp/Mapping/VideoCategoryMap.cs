﻿using BaseballApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class VideoCategoryMap : EntityTypeConfiguration<VideoCategory>
    {
        public VideoCategoryMap()
        {
            ToTable("VideoCategory");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}