﻿using BaseballApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class AchievementMap : EntityTypeConfiguration<Achievement>
    {
        public AchievementMap()
        {
            ToTable("Achievement");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(t => t.TeamId)
                .HasColumnName("TeamId")
                .IsRequired();
            Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(50)
                .IsRequired();
            Property(t => t.Image)
                .HasColumnName("Image")
                .HasMaxLength(50)
                .IsRequired();

            HasRequired(t => t.Team)
                .WithMany(t => t.Achievements)
                .HasForeignKey(d => d.TeamId)
                .WillCascadeOnDelete(false);
        }
    }
}