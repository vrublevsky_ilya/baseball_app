﻿using BaseballApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class MatchMap : EntityTypeConfiguration<Match>
    {
        public MatchMap()
        {
            ToTable("Match");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(t => t.TournamentId)
                .HasColumnName("TournamentId")
                .IsOptional();
            Property(t => t.GamePlaceId)
                .HasColumnName("GamePlaceId")
                .IsOptional();
            Property(t => t.WeatherId)
                .HasColumnName("WeatherId")
                .IsOptional();
            Property(t => t.JudgeId)
                .HasColumnName("JudgeId")
                .IsOptional();
            Property(t => t.TeamWinnerId)
                .HasColumnName("TeamWinnerId")
                .IsOptional();
            Property(t => t.StartDate)
                .HasColumnName("StartDate")
                .IsOptional();

            HasOptional(t => t.Tournament)
                .WithMany(t => t.Matches)
                .HasForeignKey(d => d.TournamentId)
                .WillCascadeOnDelete(false);
            HasOptional(t => t.GamePlace)
                .WithMany(t => t.Matches)
                .HasForeignKey(d => d.GamePlaceId)
                .WillCascadeOnDelete(false);
            HasOptional(t => t.Weather)
                .WithMany(t => t.Matches)
                .HasForeignKey(d => d.WeatherId)
                .WillCascadeOnDelete(false);
            HasOptional(t => t.Judge)
                .WithMany(t => t.Matches)
                .HasForeignKey(d => d.JudgeId)
                .WillCascadeOnDelete(false);
            HasOptional(t => t.TeamWinner)
                .WithMany(t => t.Matches)
                .HasForeignKey(d => d.TeamWinnerId)
                .WillCascadeOnDelete(false);
            HasRequired(t => t.WidgetNextMatch)
                .WithRequiredPrincipal(t => t.Match)
                .WillCascadeOnDelete(true);
        }
    }
}