﻿using BaseballApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class TeamMap : EntityTypeConfiguration<Team>
    {
        public TeamMap()
        {
            ToTable("Team");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(t => t.CityId)
                .HasColumnName("CityId")
                .IsOptional();
            Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(50)
                .IsRequired();
            Property(t => t.FullName)
                .HasColumnName("FullName")
                .HasMaxLength(50)
                .IsRequired();
            Property(t => t.Coach)
                .HasColumnName("Coach")
                .HasMaxLength(50)
                .IsOptional();
            Property(t => t.Logo)
                .HasColumnName("Logo")
                .HasMaxLength(50)
                .IsOptional();
            Property(t => t.BackgroundImage)
                .HasColumnName("BackgroundImage")
                .HasMaxLength(50)
                .IsOptional();

            HasOptional(t => t.City)
                .WithMany(t => t.Teams)
                .HasForeignKey(d => d.CityId)
                .WillCascadeOnDelete(false);
        }
    }
}