﻿using BaseballApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BaseballApp.Mapping
{
    public class JudgeMap : EntityTypeConfiguration<Judge>
    {
        public JudgeMap()
        {
            ToTable("Judge");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(t => t.CityId)
                .HasColumnName("CityId")
                .IsOptional();
            Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(50)
                .IsRequired();
            Property(t => t.Surname)
                .HasColumnName("Surname")
                .HasMaxLength(50)
                .IsRequired();

            HasOptional(t => t.City)
                .WithMany(t => t.Judges)
                .HasForeignKey(d => d.CityId)
                .WillCascadeOnDelete(false);
        }
    }
}