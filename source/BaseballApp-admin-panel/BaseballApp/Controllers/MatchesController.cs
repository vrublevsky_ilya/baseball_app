﻿using BaseballApp.Interfaces;
using BaseballApp.Repositories;
using BaseballApp.ViewModels;
using System.Web.Mvc;

namespace BaseballApp.Controllers
{
    public class MatchesController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public MatchesController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index(int id)
        {
            var match = UoW.Matches.Get(id);

            MatchVM matchVM = new MatchVM();

            matchVM.Match = match;

            return View(matchVM);
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}