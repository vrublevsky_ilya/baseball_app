﻿using BaseballApp.Interfaces;
using BaseballApp.Repositories;
using BaseballApp.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace BaseballApp.Controllers
{
    public class ApiController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public ApiController()
        {
            UoW = new EFUnitOfWork();
        }

        [HttpPost]
        public JsonResult GetInfo()
        {
            TwitterNews twitterNews = new TwitterNews();
            List<string> _tweets = twitterNews.GetTweets(5);

            var _news = UoW.NewsExtended.GetByCount(3)
                .Select(newsItem => new
                {
                    Category = newsItem.Category?.Name,
                    Title = newsItem.Title,
                    Date = newsItem.PubDate.ToLongDateString()
                });

            var data = new
            {
                Tweets = _tweets,
                News = _news
            };

            return Json(data);
        }

        [HttpPost]
        public JsonResult GetTeamStats(int id)
        {
            var teamStats = UoW.TeamStatsExtended.GetByTeam(id);

            var data = new
            {
                Years = teamStats.Select(s => s.Year),
                Victories = teamStats.Select(s => s.Victories),
                Defeats = teamStats.Select(s => s.Defeats)
            };

            return Json(data);
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}