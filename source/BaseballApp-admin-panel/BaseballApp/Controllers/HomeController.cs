﻿using BaseballApp.Interfaces;
using BaseballApp.Models;
using BaseballApp.Repositories;
using BaseballApp.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace BaseballApp.Controllers
{
    public class HomeController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public HomeController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            var news = UoW.NewsExtended.GetByCount(6);
            var newsSlides = UoW.NewsSlides.GetAll();
            var videoCategories = UoW.VideoCategories.GetAll();
            var videoSlides = UoW.VideoSlides.GetAll();
            var teamStats = UoW.TeamStatsExtended.GetByCount(6);
            var lastMatches = UoW.MatchesExtended.GetByCount(4);
            var nextMatch = UoW.WidgetNextMatch.GetAll().First().Match;
            var partners = UoW.Partners.GetAll();
            
            HomeVM homeVM = new HomeVM();

            news[3].ShortNews = news[3].ShortNews.Substring(0, 85) + "...";
            news[4].ShortNews = news[4].ShortNews.Substring(0, 85) + "...";
            news[5].ShortNews = news[5].ShortNews.Substring(0, 85) + "...";

            homeVM.News = new List<NewsItem> { news[0], news[1], news[2] };
            homeVM.OldNews = new List<NewsItem> { news[3], news[4], news[5] };
            homeVM.NewsSlides = newsSlides;
            homeVM.VideoCategories = videoCategories;
            homeVM.VideoSlides = videoSlides;
            homeVM.TeamStats = teamStats;
            homeVM.LastMatches = lastMatches;
            homeVM.NextMatch = nextMatch;
            homeVM.Partners = partners;
            
            return View(homeVM);
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}