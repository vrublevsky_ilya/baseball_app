﻿using BaseballApp.Interfaces;
using BaseballApp.Repositories;
using BaseballApp.ViewModels;
using System.Web.Mvc;

namespace BaseballApp.Controllers
{
    public class TournamentsController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public TournamentsController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index(int id)
        {
            var tournament = UoW.Tournaments.Get(id);
            var upcomingMatches = UoW.MatchesExtended.UpcomingGetByTournament(id, 8);
            var latestMatches = UoW.MatchesExtended.LatestGetByTournament(id, 5);

            TournamentVM tournamentVM = new TournamentVM();

            tournamentVM.Tournament = tournament;
            tournamentVM.UpcomingMatches = upcomingMatches;
            tournamentVM.LatestMatches = latestMatches;

            return View(tournamentVM);
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}