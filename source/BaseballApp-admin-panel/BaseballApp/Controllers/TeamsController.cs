﻿using BaseballApp.Interfaces;
using BaseballApp.Repositories;
using BaseballApp.ViewModels;
using System.Web.Mvc;

namespace BaseballApp.Controllers
{
    public class TeamsController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public TeamsController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            var teams = UoW.Teams.GetAll();

            return View(teams);
        }

        public ActionResult Team(int id)
        {
            var team = UoW.Teams.Get(id);
            var mainRosterPlayers = UoW.PlayersExtended.GetMainRosterByTeam(id);
            var tournamentsInfo = UoW.TournamentTeamsExtended.GetTournamentsInfoByTeam(id);
            var teamStats = UoW.TeamStatsExtended.GetByCount(6);
            var lastMatches = UoW.MatchesExtended.GetByTeam(id, 4);
            var achievements = UoW.AchievementsExtended.GetByTeam(id);

            TeamVM teamVM = new TeamVM();

            teamVM.Team = team;
            teamVM.Players = mainRosterPlayers;
            teamVM.TournamentsInfo = tournamentsInfo;
            teamVM.TeamStats = teamStats;
            teamVM.LastMatches = lastMatches;
            teamVM.Achievements = achievements;

            return View(teamVM);
        }

        public ActionResult TeamInfo(int id, string page)
        {
            switch (page)
            {
                case "players":
                    { 
                        var team = UoW.Teams.Get(id);
                        var players = UoW.PlayersExtended.GetByTeam(id);

                        TeamVM teamVM = new TeamVM();

                        teamVM.Team = team;
                        teamVM.Players = players;

                        return View("Players", teamVM);
                    }

                default:
                    break;
            }

            return HttpNotFound();
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}