﻿using System.Web.Mvc;

namespace BaseballApp.Helpers
{
    public static class UrlHelperExtension
    {
        public static string ActionFullPath(this UrlHelper urlHelper, string actionName, string controllerName, object routeValues = null)
        {
            string scheme = urlHelper.RequestContext.HttpContext.Request.Url.Scheme;

            return urlHelper.Action(actionName, controllerName, routeValues, scheme);
        }
    }
}