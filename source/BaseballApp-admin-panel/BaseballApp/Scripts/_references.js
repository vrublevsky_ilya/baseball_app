/// <autosync enabled="true" />
/// <reference path="additional-methods.min.js" />
/// <reference path="admin-panel.js" />
/// <reference path="app.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="chart.min.js" />
/// <reference path="init.js" />
/// <reference path="jquery.countdown.min.js" />
/// <reference path="jquery.easy-ticker.min.js" />
/// <reference path="jquery.magnific-popup.min.js" />
/// <reference path="jquery.validate.min.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-3.1.1.min.js" />
/// <reference path="jquery-3.1.1.slim.min.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <reference path="respond.matchmedia.addlistener.min.js" />
/// <reference path="respond.min.js" />
/// <reference path="slick.min.js" />
