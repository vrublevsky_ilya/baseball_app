﻿$(".news-list").slick({
    autoplay: true,
    arrows: false
});
$(".partners-list").slick({
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1
});
$(".video-list").slick({
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});

var matchDate = $(".countdown-content").data("date");
$(".countdown-content").countdown(matchDate, function (event) {
    var $this = $(this).html(event.strftime(
        "<div class=\"countdown-item\">%D <span class=\"countdown-label\">Дни</span></div>" +
        "<div class=\"countdown-item\">%H <span class=\"countdown-label\">Часы</span></div>" +
        "<div class=\"countdown-item\">%M <span class=\"countdown-label\">Мин</span></div>" +
        "<div class=\"countdown-item\">%S <span class=\"countdown-label\">Сек</span></div>"));
});

$(".video-link").magnificPopup({
    type: "iframe",
    disableOn: 700,
    preloader: false,
    fixedContentPos: true,
    removalDelay: 150,
    mainClass: "mfp-fade",
    tClose: "Закрыть (Esc)"
});