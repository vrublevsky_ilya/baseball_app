﻿$(function () {

    var pathname = $(location).attr("pathname");

    var elements = {
        wrapper:     $("#wrapper"),
        mainSidebar: $(".main-sidebar"),
        fileInput:   $("#file-input"),
        bgInput:     $("#bg-input"),
        previewImg:  $("#preview-img")
    };

    var modals = {
        deleteModal: $("#deleteModal"),
        newsCat:     $("#newsCategoryModal"),
        videoCat:    $("#videoCategoryModal"),
        gamePlace:   $("#gamePlaceModal"),
        judge:       $("#judgeModal"),
        country:     $("#countryModal"),
        city:        $("#cityModal"),
        achievement: $("#achievementModal")
    };

    var forms = {
        deleteForm:  modals.deleteModal.find("form"),
        newsCat:     modals.newsCat.find("form"),
        videoCat:    modals.videoCat.find("form"),
        gamePlace:   modals.gamePlace.find("form"),
        judge:       modals.judge.find("form"),
        country:     modals.country.find("form"),
        city:        modals.city.find("form"),
        achievement: modals.achievement.find("form")
    };

    var fixLayout;

    (fixLayout = (function() {
        return (function() {
            var sidebarHeight = elements.mainSidebar.height(),
                wrapperHeight = elements.wrapper.height();

            if (sidebarHeight <= wrapperHeight)
                elements.mainSidebar.css("height", wrapperHeight);
            else
                elements.mainSidebar.css("height", "auto");
        })();
    }))();

    $(window).resize(function() {
        fixLayout();

        if ($(this).outerWidth() > 767) {
            elements.wrapper.removeClass("show-siderbar");
            elements.wrapper.removeClass("hide-siderbar");
        }
    });

    $("#sidebar-toggle").click(function(e) {
        e.preventDefault();

        if (elements.wrapper.hasClass("show-siderbar")) {
            elements.wrapper.removeClass("show-siderbar");
            elements.wrapper.addClass("hide-siderbar");
        } else {
            elements.wrapper.removeClass("hide-siderbar");
            elements.wrapper.addClass("show-siderbar");
        }
    });

    $(".btn-logout").click(function(e) {
        e.preventDefault();

        $("#logout-form").submit();
    });

    $(".btn-delete").click(function(e) {
        e.preventDefault();

        var deleteUrl = $(this).data("url");

        forms.deleteForm.attr("action", deleteUrl);
        modals.deleteModal.modal("show");
    });

    $("#add-news-category").click(function(e) {
        e.preventDefault();

        var addUrl = $(this).data("url");

        forms.newsCat.attr("action", addUrl);
        modals.newsCat.find(".modal-title").text("Добавление категории новостей");
        modals.newsCat.find(".btn-success").text("Добавить");
        modals.newsCat.modal("show");
    });
    $("#add-video-category").click(function(e) {
        e.preventDefault();

        var addUrl = $(this).data("url");

        forms.videoCat.attr("action", addUrl);
        modals.videoCat.find(".modal-title").text("Добавление категории видео");
        modals.videoCat.find(".btn-success").text("Добавить");
        modals.videoCat.modal("show");
    });
    $("#add-game-place").click(function(e) {
        e.preventDefault();

        var addUrl = $(this).data("url");

        forms.gamePlace.attr("action", addUrl);
        modals.gamePlace.find(".modal-title").text("Добавление места проведения");
        modals.gamePlace.find(".btn-success").text("Добавить");
        modals.gamePlace.modal("show");
    });
    $("#add-judge").click(function(e) {
        e.preventDefault();

        var addUrl = $(this).data("url");

        forms.judge.attr("action", addUrl);
        modals.judge.find(".modal-title").text("Добавление судьи");
        modals.judge.find(".btn-success").text("Добавить");
        modals.judge.modal("show");
    });
    $("#add-country").click(function(e) {
        e.preventDefault();

        var addUrl = $(this).data("url");

        forms.country.attr("action", addUrl);
        modals.country.find(".modal-title").text("Добавление страны");
        modals.country.find(".btn-success").text("Добавить");
        modals.country.modal("show");
    });
    $("#add-city").click(function(e) {
        e.preventDefault();

        var addUrl = $(this).data("url");

        forms.city.attr("action", addUrl);
        modals.city.find(".modal-title").text("Добавление города");
        modals.city.find(".btn-success").text("Добавить");
        modals.city.modal("show");
    });
    $("#add-achievement").click(function(e) {
        e.preventDefault();

        var addUrl = $(this).data("url");

        forms.achievement.attr("action", addUrl);
        modals.achievement.find(".modal-title").text("Добавление достижения");
        modals.achievement.find(".btn-success").text("Добавить");
        modals.achievement.modal("show");
    });

    $(".edit-news-category").click(function(e) {
        e.preventDefault();

        var editUrl = $(this).data("url");

        var categoryName = $(this).parent().parent().children().eq(1).text();

        modals.newsCat.find("#NewsCategory").val(categoryName);

        forms.newsCat.attr("action", editUrl);
        modals.newsCat.find(".modal-title").text("Редактирование категории новостей");
        modals.newsCat.find(".btn-success").text("Сохранить");
        modals.newsCat.modal("show");
    });
    $(".edit-video-category").click(function(e) {
        e.preventDefault();

        var editUrl = $(this).data("url");

        var categoryName = $(this).parent().parent().children().eq(1).text();

        modals.videoCat.find("#VideoCategory").val(categoryName);

        forms.videoCat.attr("action", editUrl);
        modals.videoCat.find(".modal-title").text("Редактирование категории видео");
        modals.videoCat.find(".btn-success").text("Сохранить");
        modals.videoCat.modal("show");
    });
    $(".edit-game-place").click(function(e) {
        e.preventDefault();

        var editUrl = $(this).data("url");

        var gamePlaceName = $(this).parent().parent().children().eq(1).text();

        modals.gamePlace.find("#GamePlace").val(gamePlaceName);

        forms.gamePlace.attr("action", editUrl);
        modals.gamePlace.find(".modal-title").text("Редактирование места проведения");
        modals.gamePlace.find(".btn-success").text("Сохранить");
        modals.gamePlace.modal("show");
    });
    $(".edit-judge").click(function(e) {
        e.preventDefault();

        var editUrl = $(this).data("url");

        var judge    = $(this).parent().parent().children().eq(1).text(),
            cityId   = $(this).parent().parent().children().eq(1).data("id"),
            fullName = judge.split(" ");

        modals.judge.find("#JudgeName").val(fullName[1]);
        modals.judge.find("#JudgeSurname").val(fullName[2]);
        modals.judge.find("#CityId").val(cityId);

        forms.judge.attr("action", editUrl);
        modals.judge.find(".modal-title").text("Редактирование судьи");
        modals.judge.find(".btn-success").text("Сохранить");
        modals.judge.modal("show");
    });
    $(".edit-country").click(function(e) {
        e.preventDefault();

        var editUrl = $(this).data("url");

        var countryName = $(this).parent().parent().children().eq(1).text(),
            alpha2      = $(this).parent().parent().children().eq(2).text();

        modals.country.find("#NameCountry").val(countryName);
        modals.country.find("#Alpha2").val(alpha2);

        forms.country.attr("action", editUrl);
        modals.country.find(".modal-title").text("Редактирование страны");
        modals.country.find(".btn-success").text("Сохранить");
        modals.country.modal("show");
    });
    $(".edit-city").click(function(e) {
        e.preventDefault();

        var editUrl = $(this).data("url");

        var cityName  = $(this).parent().parent().children().eq(1).text(),
            countryId = $(this).parent().parent().children().eq(2).data("id");

        modals.city.find("#CountryId").val(countryId);
        modals.city.find("#NameCity").val(cityName);

        forms.city.attr("action", editUrl);
        modals.city.find(".modal-title").text("Редактирование города");
        modals.city.find(".btn-success").text("Сохранить");
        modals.city.modal("show");
    });
    $(".edit-achievement").click(function(e) {
        e.preventDefault();

        var editUrl = $(this).data("url");

        var tmp    = $(this).parent().parent().children().eq(1),
            img    = tmp.data("img"),
            name   = tmp.text(),
            teamId = $(this).parent().parent().children().eq(2).data("id");

        elements.previewImg.attr("src", "/content/img/achievements/" + img);
        elements.previewImg.attr("data-img", img);
        modals.achievement.find("#Name").val(name);
        modals.achievement.find("#TeamId").val(teamId);

        forms.achievement.attr("action", editUrl);
        modals.achievement.find(".modal-title").text("Редактирование достижения");
        modals.achievement.find(".btn-success").text("Сохранить");
        modals.achievement.modal("show");
    });

    modals.newsCat.on("hidden.bs.modal", function(e) {
        $("#NewsCategory").val("");
        $("#NewsCategory-error > span").remove();
        forms.newsCat.attr("action", "#");
    });
    modals.videoCat.on("hidden.bs.modal", function(e) {
        $("#VideoCategory").val("");
        $("#VideoCategory-error > span").remove();
        forms.videoCat.attr("action", "#");
    });
    modals.gamePlace.on("hidden.bs.modal", function(e) {
        $("#GamePlace").val("");
        $("#GamePlace-error > span").remove();
        forms.gamePlace.attr("action", "#");
    });
    modals.judge.on("hidden.bs.modal", function(e) {
        $("#JudgeName").val("");
        $("#JudgeSurname").val("");
        $("#CityId").val("");
        $("#JudgeName-error > span").remove();
        $("#JudgeSurname-error > span").remove();
        $("#CityId-error > span").remove();
        forms.judge.attr("action", "#");
    });
    modals.country.on("hidden.bs.modal", function(e) {
        $("#NameCountry").val("");
        $("#Alpha2").val("");
        $("#NameCountry-error > span").remove();
        $("#Alpha2-error > span").remove();
        forms.country.attr("action", "#");
    });
    modals.city.on("hidden.bs.modal", function(e) {
        $("#CountryId").val("");
        $("#NameCity").val("");
        $("#CountryId-error > span").remove();
        $("#NameCity-error > span").remove();
        forms.city.attr("action", "#");
    });
    modals.achievement.on("hidden.bs.modal", function(e) {
        $("#Name").val("");
        $("#TeamId").val("");
        $("#file-input").replaceWith($("#file-input").val("").clone(true));
        $("#Name-error > span").remove();
        $("#TeamId-error > span").remove();
        $("#Image-error > span").remove();
        elements.previewImg.attr("src", "/content/img/achievements/unknown.png");
        elements.previewImg.attr("data-img", "unknown.png");
        $("#form-file-info").css("display", "none");
        $("#file-info").html("");
        forms.achievement.attr("action", "#");
    });
    modals.deleteModal.on("hidden.bs.modal", function(e) {
        forms.deleteForm.attr("action", "#");
    });

    elements.fileInput.change(function() {
        if (this.files.length < 1)
            $("#remove-file").click();
        else
            renderImage(this.files[0]);
    });
    elements.bgInput.change(function() {
        if (this.files.length < 1)
            $("#remove-bg").click();
        else {
            var file = this.files[0];

            $("#form-bg-info").css("display", "block");
            $("#bg-info").html(`<strong>${file.name}</strong> (${formatBytes(file.size)})`);
        }
    });

    $("#remove-file").click(function(e) {
        e.preventDefault();

        var input = $("#file-input"),
            img   = elements.previewImg.data("img"),
            imgUrl;

        if (pathname.indexOf("teams") !== -1) {
            imgUrl = img !== "" ? `/content/img/teams/${elements.previewImg.data("img")}` : "/content/img/teams/unknown.jpg";
        }
        if (pathname.indexOf("players") !== -1) {
            imgUrl = img !== "" ? `/content/img/players/${elements.previewImg.data("img")}` : "/content/img/players/unknown.jpg";
        }
        if (pathname.indexOf("achievements") !== -1) {
            imgUrl = img !== "" ? `/content/img/achievements/${elements.previewImg.data("img")}` : "/content/img/achievements/unknown.png";
        }

        input.replaceWith(input.val("").clone(true));
        elements.previewImg.attr("src", imgUrl);
        $("#Image-error > span").remove();
        $("#form-file-info").css("display", "none");
        $("#file-info").html("");
    });
    $("#remove-bg").click(function(e) {
        e.preventDefault();

        var input = $("#bg-input");

        input.replaceWith(input.val("").clone(true));
        $("#form-bg-info").css("display", "none");
        $("#bg-info").html("");
    });

    function renderImage(file) {
        var reader = new FileReader();

        reader.onload = function(event) {
            $("#preview-img").attr("src", event.target.result);
        };
        reader.readAsDataURL(file);

        $("#form-file-info").css("display", "block");
        $("#file-info").html(`<strong>${file.name}</strong> (${formatBytes(file.size)})`);
    }

    function formatBytes(bytes) {
        if (bytes === 0) return "0 Bytes";

        var k     = 1024,
            dm    = 1,
            sizes = ["Bytes", "KB", "MB", "GB"],
            i     = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
    }

});