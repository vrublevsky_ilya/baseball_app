﻿$(window).on("load", function () {
    var preloader = $(".preloader"),
        cube = preloader.find(".sk-folding-cube");

    preloader.fadeOut();
    cube.delay(350).fadeOut("slow");
});

$(".ticker-wrap").easyTicker({
    interval: 4000,
    visible: 1
});

$.ajax({
    cache: false,
    type: "POST",
    url: location.origin + "/api/getinfo",
    dataType: "json",
    success: function (data) {
        $.each(data.Tweets, function (i, val) {
            $("#twitter-news").append(`<li>${val}</li>`);
        });

        $.each(data.News, function (i, val) {
            if (val.Category !== null) {
                $("#footer-news").append(
                    "<li>" +
                        `<div class="news-cat"><span>${val.Category}</span></div>` +
                        `<h6 class="news-box-title"><a href="#">${val.Title}</a></h6>` +
                        `<time>${val.Date}</time>` +
                    "</li>"
                );
            } else {
                $("#footer-news").append(
                    "<li>" +
                        `<h6 class="news-box-title"><a href="#">${val.Title}</a></h6>` +
                        `<time>${val.Date}</time>` +
                    "</li>"
                );
            }
        });
    }
});

if ($("canvas").is("#chart-games-history")) {
    $.ajax({
        cache: false,
        type: "POST",
        url: location.origin + "/api/getteamstats/" + $("#chart-games-history").data("id"),
        dataType: "json",
        success: function (data) {
            createChart(data);
        }
    });

    function createChart(data) {
        var ctx = $("#chart-games-history");
        ctx[0].height = 309;

        var chart = new Chart(ctx, {
            type: "bar",
            data: {
                labels: data.Years,
                datasets: [{
                    label: "ПОБЕДЫ",
                    data: data.Victories,
                    backgroundColor: "#ff2828"
                }, {
                    label: "ПОРАЖЕНИЯ",
                    data: data.Defeats,
                    backgroundColor: "#38a9ff"
                }]
            },
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    backgroundColor: "rgba(49, 64, 75, 0.8)",
                    titleFontSize: 0,
                    titleSpacing: 0,
                    titleMarginBottom: 0,
                    bodyFontSize: 10,
                    bodySpacing: 0,
                    cornerRadius: 2,
                    xPadding: 10,
                    displayColors: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                            color: "rgba(255, 255, 255, 0)"
                        },
                        ticks: {
                            fontColor: "#9a9da2",
                            fontSize: 12
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: "rgba(255, 255, 255, 0)"
                        },
                        ticks: {
                            beginAtZero: true,
                            fontColor: "#9a9da2",
                            fontSize: 12
                        }
                    }]
                }
            }
        });
    }

    $(".achievements-list").slick({
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });
}