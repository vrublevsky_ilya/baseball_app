﻿using BaseballApp.Models;
using Microsoft.AspNet.Identity;

namespace BaseballApp.Identity
{
    public class AppUserManager : UserManager<User>
    {
        public AppUserManager(IUserStore<User> store) : base(store)
        {
        }
    }
}