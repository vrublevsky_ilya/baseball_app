﻿using System.Web.Optimization;

namespace BaseballApp
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/additional-methods.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminpanel").Include(
                      "~/Scripts/admin-panel.js"));

            bundles.Add(new ScriptBundle("~/bundles/ticker").Include(
                      "~/Scripts/jquery.easy-ticker.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/mainpage").Include(
                      "~/Scripts/slick.min.js",
                      "~/Scripts/jquery.countdown.min.js",
                      "~/Scripts/jquery.magnific-popup.min.js",
                      "~/Scripts/init.js"));

            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                      "~/Scripts/app.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.css",
                      "~/Content/css/font-awesome.css"));

            bundles.Add(new StyleBundle("~/Content/css/adminpanel").Include(
                      "~/Content/css/admin-panel.css"));

            bundles.Add(new StyleBundle("~/Content/css/mainpage").Include(
                      "~/Content/css/slick.css",
                      "~/Content/css/slick-theme.css",
                      "~/Content/css/magnific-popup.css"));

            bundles.Add(new StyleBundle("~/Content/css/site").Include(
                      "~/Content/css/styles.css"));
        }
    }
}