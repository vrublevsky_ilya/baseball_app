﻿using Kaliko.ImageLibrary;
using System;
using System.Drawing.Imaging;
using System.IO;

namespace BaseballApp.Services
{
    public static class Image
    {
        private static string CreateFileName(string fileName)
        {
            string timeStamp = DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds.ToString().Replace(",", "");

            return $"{Path.GetFileNameWithoutExtension(fileName)}-{timeStamp}{Path.GetExtension(fileName)}";
        }

        public static string ResizeAndSave(Stream stream, string fileName, string path)
        {
            string _fileName = CreateFileName(fileName);

            using (stream)
            {
                var image215px = new KalikoImage(stream);
                var image88px = new KalikoImage(stream);
                var image28px = new KalikoImage(stream);

                image215px.Resize(215, 215);
                image88px.Resize(88, 88);
                image28px.Resize(28, 28);

                image215px.SavePng($"{path}/215-{_fileName}");
                image88px.SavePng($"{path}/88-{_fileName}");
                image28px.SavePng($"{path}/28-{_fileName}");
            }

            return _fileName;
        }

        public static string Save(Stream stream, string fileName, string path, bool useEncoder, string format = "jpg")
        {
            return useEncoder ? SaveWithEncoder(stream, fileName, path, format) : SaveWithoutEncoder(stream, fileName, path, format);
        }

        private static string SaveWithoutEncoder(Stream stream, string fileName, string path, string format)
        {
            string _fileName = CreateFileName(fileName);

            using (stream)
            {
                var image = new KalikoImage(stream);

                switch (format)
                {
                    case "png":
                        image.SavePng(path + _fileName);
                        break;

                    default:
                        image.SaveJpg(path + _fileName, 100, true);
                        break;
                }
            }

            return _fileName;
        }

        private static string SaveWithEncoder(Stream stream, string fileName, string path, string format)
        {
            string _fileName = CreateFileName(fileName);

            using (stream)
            {
                ImageCodecInfo encoder;

                switch (format)
                {
                    case "png":
                        encoder = GetEncoder(ImageFormat.Png);
                        break;

                    default:
                        encoder = GetEncoder(ImageFormat.Jpeg);
                        break;
                }

                EncoderParameters encoderParams = new EncoderParameters(1);
                EncoderParameter encoderParam = new EncoderParameter(Encoder.Quality, 100L);
                encoderParams.Param[0] = encoderParam;

                using (System.Drawing.Image image = System.Drawing.Image.FromStream(stream))
                {
                    image.Save(path + _fileName, encoder, encoderParams);
                }
            }

            return _fileName;
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            return null;
        }
    }
}