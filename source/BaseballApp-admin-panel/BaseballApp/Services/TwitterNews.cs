﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Tweetinvi;
using Tweetinvi.Models;

namespace BaseballApp.Services
{
    public class TwitterNews
    {
        const string CONSUMER_KEY = "I9XauaukRFdRBrAjBRr3y9r2e";
        const string CONSUMER_SECRET = "vBBUi5v8rYN18jdJlr8C2vsDvQcx6QBqO3BMlrhe5i2yPIM2Pl";
        const string ACCESS_TOKEN = "863363466161160193-HCCBMkQXiRbuqTyWk1mc7zUGSFQmTSB";
        const string ACCESS_TOKEN_SECRET = "7NhMMAweizZ3eD0RwSaYvVzrLinXDtOFuqg3gvYVJ2yms";

        public List<string> GetTweets(int count)
        {
            var creds = new TwitterCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

            var user = User.GetAuthenticatedUser(creds);

            var tweets = Auth.ExecuteOperationWithCredentials(creds, () =>
            {
                return Timeline.GetUserTimeline(user, count);
            });

            List<string> _tweets = new List<string>();
            foreach (var tweet in tweets)
            {
                _tweets.Add(CreateMarkup(tweet.Text));
            }

            return _tweets;
        }

        private string CreateMarkup(string str)
        {
            StringBuilder sb = new StringBuilder(str);

            Regex userName = new Regex(@"@\w*", RegexOptions.Compiled);
            Regex link = new Regex(@"https://t.co/\w*", RegexOptions.Compiled);
            Regex hashTag = new Regex(@"#\w*", RegexOptions.Compiled);

            MatchCollection userNameMatches = userName.Matches(sb.ToString());
            MatchCollection linkMatches = link.Matches(sb.ToString());
            MatchCollection hashTagMatches = hashTag.Matches(sb.ToString());

            if (userNameMatches.Count > 0)
            {
                foreach (Match match in userNameMatches)
                {
                    sb = sb.Replace(match.Value, $"<a href=\"https://twitter.com/{match.Value.Remove(0, 1)}\" target=\"_blank\">{match.Value}</a>");
                }
            }

            if (linkMatches.Count > 0)
            {
                foreach (Match match in linkMatches)
                {
                    sb = sb.Replace(match.Value, $"<a href=\"{match.Value}\" target=\"_blank\">{match.Value}</a>");
                }
            }

            if (hashTagMatches.Count > 0)
            {
                foreach (Match match in hashTagMatches)
                {
                    sb = sb.Replace(match.Value, $"<a href=\"https://twitter.com/hashtag/{match.Value.Remove(0, 1)}?src=hash\" target=\"_blank\">{match.Value}</a>");
                }
            }

            return sb.ToString();
        }
    }
}