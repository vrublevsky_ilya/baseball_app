﻿using BaseballApp.Models;
using BaseballApp.Repositories;
using System;

namespace BaseballApp.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Achievement> Achievements { get; }
        IRepository<Category> Categories { get; }
        IRepository<City> Cities { get; }
        IRepository<Country> Countries { get; }
        IRepository<GamePlace> GamePlaces { get; }
        IRepository<Judge> Judges { get; }
        IRepository<Match> Matches { get; }
        IRepository<MatchTeams> MatchTeams { get; }
        IRepository<NewsItem> News { get; }
        IRepository<NewsSlide> NewsSlides { get; }
        IRepository<Partner> Partners { get; }
        IRepository<Player> Players { get; }
        IRepository<Position> Positions { get; }
        IRepository<Team> Teams { get; }
        IRepository<TeamStats> TeamStats { get; }
        IRepository<Tournament> Tournaments { get; }
        IRepository<TournamentTeams> TournamentTeams { get; }
        IRepository<VideoCategory> VideoCategories { get; }
        IRepository<VideoSlide> VideoSlides { get; }
        IRepository<Weather> Weather { get; }
        IRepository<WidgetNextMatch> WidgetNextMatch { get; }

        AchievementRepository AchievementsExtended { get; }
        NewsRepository NewsExtended { get; }
        TeamStatsRepository TeamStatsExtended { get; }
        MatchRepository MatchesExtended { get; }
        PlayerRepository PlayersExtended { get; }
        TournamentTeamsRepository TournamentTeamsExtended { get; }

        void Save();
    }
}