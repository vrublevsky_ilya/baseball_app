﻿using BaseballApp.Identity;
using Microsoft.Owin.Security;
using System;

namespace BaseballApp.Interfaces
{
    interface IIdentityUnitOfWork : IDisposable
    {
        IAuthenticationManager AppAuthManager { get; set; }
        AppUserManager AppUserManager { get; }
        AppSignInManager AppSignInManager { get; }
    }
}