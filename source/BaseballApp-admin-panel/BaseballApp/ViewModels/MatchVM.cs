﻿using BaseballApp.Models;

namespace BaseballApp.ViewModels
{
    public class MatchVM
    {
        public Match Match { get; set; }
    }
}