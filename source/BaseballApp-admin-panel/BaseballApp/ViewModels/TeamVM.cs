﻿using BaseballApp.Models;
using System.Collections.Generic;

namespace BaseballApp.ViewModels
{
    public class TeamVM
    {
        public Team Team { get; set; }

        public IEnumerable<Player> Players { get; set; }
        public IEnumerable<TournamentTeams> TournamentsInfo { get; set; }
        public IEnumerable<TeamStats> TeamStats { get; set; }
        public IEnumerable<Match> LastMatches { get; set; }
        public IEnumerable<Achievement> Achievements { get; set; }
    }
}