﻿using BaseballApp.Models;
using System.Collections.Generic;

namespace BaseballApp.ViewModels
{
    public class HomeVM
    {
        public IEnumerable<NewsItem> News { get; set; }
        public IEnumerable<NewsItem> OldNews { get; set; }
        public IEnumerable<NewsSlide> NewsSlides { get; set; }
        public IEnumerable<VideoCategory> VideoCategories { get; set; }
        public IEnumerable<VideoSlide> VideoSlides { get; set; }
        public IEnumerable<TeamStats> TeamStats { get; set; }
        public IEnumerable<Match> LastMatches { get; set; }
        public IEnumerable<Partner> Partners { get; set; }

        public Match NextMatch { get; set; }
    }
}