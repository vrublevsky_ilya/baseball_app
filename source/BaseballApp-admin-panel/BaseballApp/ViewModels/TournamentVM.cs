﻿using BaseballApp.Models;
using System.Collections.Generic;

namespace BaseballApp.ViewModels
{
    public class TournamentVM
    {
        public Tournament Tournament { get; set; }

        public IEnumerable<Match> UpcomingMatches { get; set; }
        public IEnumerable<Match> LatestMatches { get; set; }
    }
}