﻿using BaseballApp.Areas.Admin.ViewModels;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using BaseballApp.Repositories;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.Controllers
{
    [Authorize]
    public class GameInfoController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public GameInfoController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            var gamePlaces = UoW.GamePlaces.GetAll();
            var judges = UoW.Judges.GetAll();
            var cities = UoW.Cities.GetAll();

            GameInfoVM gameInfoVM = new GameInfoVM
            {
                GamePlaces = gamePlaces,
                Judges = judges,
                JudgeVM = new JudgeVM { Cities = new SelectList(cities, "Id", "Name") }
            };

            return View(gameInfoVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateGamePlace([Bind(Include = "Name")] GamePlaceVM gamePlaceVM)
        {
            if (ModelState.IsValid)
            {
                GamePlace gamePlace = new GamePlace { Name = gamePlaceVM.Name };

                UoW.GamePlaces.Create(gamePlace);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateJudge([Bind(Include = "CityId, Name, Surname")] JudgeVM judgeVM)
        {
            if (ModelState.IsValid)
            {
                Judge judge = new Judge
                {
                    CityId = judgeVM.CityId,
                    Name = judgeVM.Name,
                    Surname = judgeVM.Surname
                };

                UoW.Judges.Create(judge);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditGamePlace([Bind(Include = "Name")] GamePlaceVM gamePlaceVM, int id)
        {
            var gamePlace = UoW.GamePlaces.Get(id);
            if (gamePlace == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                gamePlace.Name = gamePlaceVM.Name;

                UoW.GamePlaces.Update(gamePlace);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditJudge([Bind(Include = "CityId, Name, Surname")] JudgeVM judgeVM, int id)
        {
            var judge = UoW.Judges.Get(id);
            if (judge == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                judge.CityId = judgeVM.CityId;
                judge.Name = judgeVM.Name;
                judge.Surname = judgeVM.Surname;

                UoW.Judges.Update(judge);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteGamePlace(int id)
        {
            UoW.GamePlaces.Delete(id);
            UoW.Save();

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteJudge(int id)
        {
            UoW.Judges.Delete(id);
            UoW.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}