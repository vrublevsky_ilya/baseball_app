﻿using BaseballApp.Areas.Admin.ViewModels;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using BaseballApp.Repositories;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.Controllers
{
    [Authorize]
    public class CategoriesController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public CategoriesController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            var newsCategories = UoW.Categories.GetAll();
            var videoCategories = UoW.VideoCategories.GetAll();

            CategoryVM categoryVM = new CategoryVM
            {
                NewsCategories = newsCategories,
                VideoCategories = videoCategories
            };

            return View(categoryVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewsCategory([Bind(Include = "Name")] NewsCategoryVM newsCategoryVM)
        {
            if (ModelState.IsValid)
            {
                Category category = new Category { Name = newsCategoryVM.Name };

                UoW.Categories.Create(category);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateVideoCategory([Bind(Include = "Name")] VideoCategoryVM videoCategoryVM)
        {
            if (ModelState.IsValid)
            {
                VideoCategory videoCategory = new VideoCategory { Name = videoCategoryVM.Name };

                UoW.VideoCategories.Create(videoCategory);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditNewsCategory([Bind(Include = "Name")] NewsCategoryVM newsCategoryVM, int id)
        {
            var category = UoW.Categories.Get(id);
            if (category == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                category.Name = newsCategoryVM.Name;

                UoW.Categories.Update(category);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditVideoCategory([Bind(Include = "Name")] VideoCategoryVM videoCategoryVM, int id)
        {
            var videoCategory = UoW.VideoCategories.Get(id);
            if (videoCategory == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                videoCategory.Name = videoCategoryVM.Name;

                UoW.VideoCategories.Update(videoCategory);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteNewsCategory(int id)
        {
            UoW.Categories.Delete(id);
            UoW.Save();

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteVideoCategory(int id)
        {
            UoW.VideoCategories.Delete(id);
            UoW.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}