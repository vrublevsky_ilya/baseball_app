﻿using BaseballApp.Areas.Admin.ViewModels;
using BaseballApp.Interfaces;
using BaseballApp.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        IIdentityUnitOfWork UoW { get; }

        public AuthController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("index", "categories");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(LoginVM loginVM, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                UoW.AppAuthManager = HttpContext.GetOwinContext().Authentication;

                var result = await UoW.AppSignInManager.PasswordSignInAsync(loginVM.Login, loginVM.Password, loginVM.RememberMe, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        if (returnUrl != null)
                            return Redirect(returnUrl);
                        return RedirectToAction("index", "categories");
                    default:
                        ModelState.AddModelError("", "Неверный логин или пароль");
                        return View(loginVM);
                }
            }

            return View(loginVM);
        }



        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("index");
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}