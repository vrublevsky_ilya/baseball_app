﻿using BaseballApp.Areas.Admin.ViewModels;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using BaseballApp.Repositories;
using BaseballApp.Services;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.Controllers
{
    [Authorize]
    public class AchievementsController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public AchievementsController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            var achievements = UoW.Achievements.GetAll();
            var teams = UoW.Teams.GetAll();

            AchievementVM achievementVM = new AchievementVM
            {
                Achievements = achievements,
                Teams = new SelectList(teams, "Id", "Name")
            };

            return View(achievementVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TeamId, Name, Image")] AchievementVM achievementVM)
        {
            if (ModelState.IsValid)
            {
                string fileName = "unknown.png";
                if (achievementVM.Image != null)
                {
                    string path = Server.MapPath("~/Content/img/achievements/");
                    fileName = Image.Save(achievementVM.Image.InputStream, achievementVM.Image.FileName, path, true, "png");
                }

                Achievement achievement = new Achievement
                {
                    TeamId = achievementVM.TeamId,
                    Name = achievementVM.Name,
                    Image = fileName
                };

                UoW.Achievements.Create(achievement);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TeamId, Name, Image")] AchievementVM achievementVM, int id)
        {
            var achievement = UoW.Achievements.Get(id);
            if (achievement == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                string fileName = null;
                if (achievementVM.Image != null)
                {
                    string path = Server.MapPath("~/Content/img/achievements/");
                    fileName = Image.Save(achievementVM.Image.InputStream, achievementVM.Image.FileName, path, true, "png");
                }

                achievement.TeamId = achievementVM.TeamId;
                achievement.Name = achievementVM.Name;
                if (fileName != null) achievement.Image = fileName;

                UoW.Achievements.Update(achievement);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            UoW.Achievements.Delete(id);
            UoW.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}