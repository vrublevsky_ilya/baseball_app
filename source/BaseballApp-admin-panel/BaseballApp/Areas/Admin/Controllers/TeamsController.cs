﻿using BaseballApp.Areas.Admin.ViewModels;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using BaseballApp.Repositories;
using BaseballApp.Services;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.Controllers
{
    [Authorize]
    public class TeamsController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public TeamsController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            var teams = UoW.Teams.GetAll().Reverse();

            var teamsVM = new List<IndexTeamVM>();
            foreach (var item in teams)
            {
                teamsVM.Add(new IndexTeamVM
                {
                    Id = item.Id,
                    Name = item.FullName,
                    Coach = item.Coach,
                    PlayersCount = item.Players.Count,
                    CityName = item.City?.Name,
                    CountryAlpha2 = item.City?.Country?.Alpha2 ?? "unknown"
                });
            }

            return View(teamsVM);
        }

        public ActionResult Create()
        {
            CreateTeamVM teamVM = new CreateTeamVM { Cities = new SelectList(UoW.Cities.GetAll(), "Id", "Name") };

            return View(teamVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "Id, Logo, BackgroundImage, Cities")] CreateTeamVM teamVM, HttpPostedFileBase logo, HttpPostedFileBase bgImage)
        {
            string logoName = null;
            string bgImageName = null;

            if (logo != null)
            {
                if (Path.GetExtension(logo.FileName) == ".png")
                {
                    string path = Server.MapPath("~/Content/img/teams/");
                    logoName = Image.ResizeAndSave(logo.InputStream, logo.FileName, path);
                }
                else
                {
                    ModelState.AddModelError("logo", "Выберите логотип в формате PNG");
                }
            }

            if (bgImage != null)
            {
                if (Path.GetExtension(bgImage.FileName) == ".jpg")
                {
                    string path = Server.MapPath("~/Content/img/teams/");
                    bgImageName = Image.Save(bgImage.InputStream, bgImage.FileName, path, true);
                }
                else
                {
                    ModelState.AddModelError("bgImage", "Выберите задний фон в формате JPG");
                }
            }

            if (ModelState.IsValid)
            {
                Team team = new Team
                {
                    CityId = teamVM.CityId,
                    Name = teamVM.Name,
                    FullName = teamVM.FullName,
                    Coach = teamVM.Coach,
                    Logo = logoName,
                    BackgroundImage = bgImageName
                };

                UoW.Teams.Create(team);
                UoW.Save();

                return RedirectToAction("Index");
            }

            teamVM.Cities = new SelectList(UoW.Cities.GetAll(), "Id", "Name", teamVM.CityId);

            return View(teamVM);
        }

        public ActionResult Edit(int id)
        {
            Team team = UoW.Teams.Get(id);
            if (team == null)
            {
                return HttpNotFound();
            }

            CreateTeamVM teamVM = new CreateTeamVM
            {
                Id = team.Id,
                Name = team.Name,
                FullName = team.FullName,
                Coach = team.Coach,
                Logo = team.Logo != null ? $"215-{team.Logo}" : "unknown.jpg",
                BackgroundImage = team.BackgroundImage != null ? team.BackgroundImage : "default-bg.jpg",
                Cities = new SelectList(UoW.Cities.GetAll(), "Id", "Name", team.CityId)
            };

            return View(teamVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Exclude = "BackgroundImage, Cities")] CreateTeamVM teamVM, HttpPostedFileBase logo, HttpPostedFileBase bgImage)
        {
            string logoName = null;
            string bgImageName = null;

            if (logo != null)
            {
                if (Path.GetExtension(logo.FileName) == ".png")
                {
                    string path = Server.MapPath("~/Content/img/teams/");
                    logoName = Image.ResizeAndSave(logo.InputStream, logo.FileName, path);
                }
                else
                {
                    ModelState.AddModelError("logo", "Выберите логотип в формате PNG");
                }
            }

            if (bgImage != null)
            {
                if (Path.GetExtension(bgImage.FileName) == ".jpg")
                {
                    string path = Server.MapPath("~/Content/img/teams/");
                    bgImageName = Image.Save(bgImage.InputStream, bgImage.FileName, path, true);
                }
                else
                {
                    ModelState.AddModelError("bgImage", "Выберите задний фон в формате JPG");
                }
            }

            if (ModelState.IsValid)
            {
                Team team = UoW.Teams.Get(teamVM.Id);

                team.CityId = teamVM.CityId;
                team.Name = teamVM.Name;
                team.FullName = teamVM.FullName;
                team.Coach = teamVM.Coach;
                if (logoName != null) team.Logo = logoName;
                if (bgImageName != null) team.BackgroundImage = bgImageName;

                UoW.Teams.Update(team);
                UoW.Save();

                return RedirectToAction("Index");
            }

            teamVM.Logo = teamVM.Logo != null ? teamVM.Logo : "unknown.jpg";
            teamVM.Cities = new SelectList(UoW.Cities.GetAll(), "Id", "Name", teamVM.CityId);

            return View(teamVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            UoW.Teams.Delete(id);
            UoW.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}