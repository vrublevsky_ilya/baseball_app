﻿using BaseballApp.Areas.Admin.ViewModels;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using BaseballApp.Repositories;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.Controllers
{
    [Authorize]
    public class LocationsController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public LocationsController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            var countries = UoW.Countries.GetAll();
            var cities = UoW.Cities.GetAll();

            LocationVM locationVM = new LocationVM
            {
                Countries = countries,
                Cities = cities,
                CityVM = new CityVM { Countries = new SelectList(countries, "Id", "Name") }
            };

            return View(locationVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCountry([Bind(Include = "NameCountry, Alpha2")] CountryVM countryVM)
        {
            if (ModelState.IsValid)
            {
                Country country = new Country
                {
                    Name = countryVM.NameCountry,
                    Alpha2 = countryVM.Alpha2
                };

                UoW.Countries.Create(country);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCity([Bind(Include = "CountryId, NameCity")] CityVM cityVM)
        {
            if (ModelState.IsValid)
            {
                City city = new City
                {
                    CountryId = cityVM.CountryId,
                    Name = cityVM.NameCity
                };

                UoW.Cities.Create(city);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCountry([Bind(Include = "NameCountry, Alpha2")] CountryVM countryVM, int id)
        {
            var country = UoW.Countries.Get(id);
            if (country == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                country.Name = countryVM.NameCountry;
                country.Alpha2 = countryVM.Alpha2;

                UoW.Countries.Update(country);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCity([Bind(Include = "CountryId, NameCity")] CityVM cityVM, int id)
        {
            var city = UoW.Cities.Get(id);
            if (city == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                city.CountryId = cityVM.CountryId;
                city.Name = cityVM.NameCity;

                UoW.Cities.Update(city);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCountry(int id)
        {
            UoW.Countries.Delete(id);
            UoW.Save();

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCity(int id)
        {
            UoW.Cities.Delete(id);
            UoW.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}