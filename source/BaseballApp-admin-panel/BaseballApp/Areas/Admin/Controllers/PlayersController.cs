﻿using BaseballApp.Areas.Admin.ViewModels;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using BaseballApp.Repositories;
using BaseballApp.Services;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.Controllers
{
    [Authorize]
    public class PlayersController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public PlayersController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            var players = UoW.Players.GetAll().Reverse();

            var playersVM = new List<IndexPlayerVM>();
            foreach (var item in players)
            {
                playersVM.Add(new IndexPlayerVM
                {
                    Id = item.Id,
                    Number = item.Number,
                    Player = $"{item.Name} {item.Surname}",
                    Team = item.Team?.Name,
                    Position = item.Position != null ? $"{item.Position?.Name} ({item.Position?.Abbreviation})" : "",
                    CityName = item.City?.Name,
                    CountryAlpha2 = item.City?.Country?.Alpha2 ?? "unknown"
                });
            }

            return View(playersVM);
        }

        public ActionResult Create()
        {
            CreatePlayerVM playerVM = new CreatePlayerVM
            {
                Cities = new SelectList(UoW.Cities.GetAll(), "Id", "Name"),
                Positions = new SelectList(UoW.Positions.GetAll(), "Id", "Name"),
                Teams = new SelectList(UoW.Teams.GetAll(), "Id", "Name")
            };

            return View(playerVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "Id, Photo, Cities, Positions, Teams, Sides")] CreatePlayerVM playerVM, HttpPostedFileBase file)
        {
            string fileName = null;
            if (file != null)
            {
                if (Path.GetExtension(file.FileName) == ".jpg")
                {
                    string path = Server.MapPath("~/Content/img/players/");
                    fileName = Image.Save(file.InputStream, file.FileName, path, true);
                }
                else
                {
                    ModelState.AddModelError("file", "Выберите изображение в формате JPG");
                }
            }

            if (ModelState.IsValid)
            {
                Player player = new Player
                {
                    CityId = playerVM.CityId,
                    PositionId = playerVM.PositionId,
                    TeamId = playerVM.TeamId,
                    Name = playerVM.Name,
                    Surname = playerVM.Surname,
                    Birthday = playerVM.Birthday,
                    Number = playerVM.Number,
                    Height = playerVM.Height,
                    Weight = playerVM.Weight,
                    Attack = playerVM.Attack,
                    Protection = playerVM.Protection,
                    MainRoster = playerVM.MainRoster,
                    DebutMajorLeague = playerVM.DebutMajorLeague,
                    PlaceOfStudy = playerVM.PlaceOfStudy,
                    Photo = fileName
                };

                UoW.Players.Create(player);
                UoW.Save();

                return RedirectToAction("Index");
            }

            playerVM.Cities = new SelectList(UoW.Cities.GetAll(), "Id", "Name", playerVM.CityId);
            playerVM.Positions = new SelectList(UoW.Positions.GetAll(), "Id", "Name", playerVM.PositionId);
            playerVM.Teams = new SelectList(UoW.Teams.GetAll(), "Id", "Name", playerVM.TeamId);

            return View(playerVM);
        }

        public ActionResult Edit(int id)
        {
            Player player = UoW.Players.Get(id);
            if (player == null)
            {
                return HttpNotFound();
            }

            CreatePlayerVM playerVM = new CreatePlayerVM
            {
                Id = player.Id,
                Name = player.Name,
                Surname = player.Surname,
                Birthday = player.Birthday,
                Number = player.Number,
                Height = player.Height,
                Weight = player.Weight,
                Attack = player.Attack,
                Protection = player.Protection,
                MainRoster = player.MainRoster,
                DebutMajorLeague = player.DebutMajorLeague,
                PlaceOfStudy = player.PlaceOfStudy,
                Photo = player.Photo != null ? player.Photo : "unknown.jpg",
                Cities = new SelectList(UoW.Cities.GetAll(), "Id", "Name", player.CityId),
                Positions = new SelectList(UoW.Positions.GetAll(), "Id", "Name", player.PositionId),
                Teams = new SelectList(UoW.Teams.GetAll(), "Id", "Name", player.TeamId)
            };

            return View(playerVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Exclude = "Cities, Positions, Teams, Sides")] CreatePlayerVM playerVM, HttpPostedFileBase file)
        {
            string fileName = null;

            if (file != null)
            {
                if (Path.GetExtension(file.FileName) == ".jpg")
                {
                    string path = Server.MapPath("~/Content/img/players/");
                    fileName = Image.Save(file.InputStream, file.FileName, path, true);
                }
                else
                {
                    ModelState.AddModelError("file", "Выберите изображение в формате JPG");
                }
            }

            if (ModelState.IsValid)
            {
                Player player = UoW.Players.Get(playerVM.Id);

                player.CityId = playerVM.CityId;
                player.PositionId = playerVM.PositionId;
                player.TeamId = playerVM.TeamId;
                player.Name = playerVM.Name;
                player.Surname = playerVM.Surname;
                player.Birthday = playerVM.Birthday;
                player.Number = playerVM.Number;
                player.Height = playerVM.Height;
                player.Weight = playerVM.Weight;
                player.Attack = playerVM.Attack;
                player.Protection = playerVM.Protection;
                player.MainRoster = playerVM.MainRoster;
                player.DebutMajorLeague = playerVM.DebutMajorLeague;
                player.PlaceOfStudy = playerVM.PlaceOfStudy;
                if (fileName != null) player.Photo = fileName;

                UoW.Players.Update(player);
                UoW.Save();

                return RedirectToAction("Index");
            }

            playerVM.Photo = playerVM.Photo != null ? playerVM.Photo : "unknown.jpg";
            playerVM.Cities = new SelectList(UoW.Cities.GetAll(), "Id", "Name", playerVM.CityId);
            playerVM.Positions = new SelectList(UoW.Positions.GetAll(), "Id", "Name", playerVM.PositionId);
            playerVM.Teams = new SelectList(UoW.Teams.GetAll(), "Id", "Name", playerVM.TeamId);

            return View(playerVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            UoW.Players.Delete(id);
            UoW.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}