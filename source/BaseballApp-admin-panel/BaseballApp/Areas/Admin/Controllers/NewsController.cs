﻿using BaseballApp.Areas.Admin.ViewModels;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using BaseballApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.Controllers
{
    [Authorize]
    public class NewsController : Controller
    {
        IUnitOfWork UoW { get; set; }

        public NewsController()
        {
            UoW = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            var news = UoW.News.GetAll();

            List<IndexNewsItemVM> newsVM = new List<IndexNewsItemVM>();
            foreach (var item in news)
            {
                newsVM.Add(new IndexNewsItemVM
                {
                    Id = item.Id,
                    Title = item.Title,
                    PubDate = item.PubDate,
                    CategoryName = item.Category?.Name
                });
            }

            return View(newsVM);
        }

        public ActionResult Create()
        {
            CreateNewsItemVM newsVM = new CreateNewsItemVM { Categories = new SelectList(UoW.Categories.GetAll(), "Id", "Name") };

            return View(newsVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "Id, Categories")] CreateNewsItemVM newsVM)
        {
            if (ModelState.IsValid)
            {
                var categories = UoW.Categories.GetAll();

                NewsItem news = new NewsItem
                {
                    CategoryId = newsVM.CategoryId,
                    Title = newsVM.Title,
                    Category = categories.FirstOrDefault(x=>x.Id == newsVM.CategoryId),
                    ShortNews = newsVM.ShortNews,
                    FullNews = newsVM.FullNews,
                    PubDate = DateTime.Now,
                    Image = string.Empty,
                };

                UoW.News.Create(news);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return View(newsVM);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            NewsItem news = UoW.News.Get((int)id);
            if (news == null)
            {
                return HttpNotFound();
            }

            CreateNewsItemVM newsVM = new CreateNewsItemVM
            {
                Id = news.Id,
                Title = news.Title,
                ShortNews = news.ShortNews,
                FullNews = news.FullNews,
                Categories = new SelectList(UoW.Categories.GetAll(), "Id", "Name", news.CategoryId)
            };

            return View(newsVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Exclude = "Categories")] CreateNewsItemVM newsVM)
        {
            if (ModelState.IsValid)
            {
                NewsItem news = UoW.News.Get(newsVM.Id);
                news.CategoryId = newsVM.CategoryId;
                news.Title = newsVM.Title;
                news.ShortNews = newsVM.ShortNews;
                news.FullNews = newsVM.FullNews;

                UoW.News.Update(news);
                UoW.Save();

                return RedirectToAction("Index");
            }

            return View(newsVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UoW.News.Delete((int)id);
            UoW.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }
    }
}