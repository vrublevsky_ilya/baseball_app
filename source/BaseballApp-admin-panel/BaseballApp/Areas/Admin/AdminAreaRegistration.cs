﻿using System.Web.Mvc;

namespace BaseballApp.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            #region Routes for Authentication
            context.MapRoute(
                "Admin_AuthLogIn",
                "admin",
                new { controller = "Auth", action = "Index" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_AuthLogOut",
                "admin/logout",
                new { controller = "Auth", action = "Logout" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            #endregion

            #region Routes for Players
            context.MapRoute(
                "Admin_Players",
                "admin/players",
                new { controller = "Players", action = "Index" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_AddPlayer",
                "admin/players/add",
                new { controller = "Players", action = "Create" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_EditPlayer",
                "admin/players/edit/{id}",
                new { controller = "Players", action = "Edit" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_DeletePlayer",
                "admin/players/delete/{id}",
                new { controller = "Players", action = "Delete" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            #endregion

            #region Routes for News
            context.MapRoute(
                "Admin_News",
                "admin/news",
                new { controller = "News", action = "Index" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_AddNews",
                "admin/news/add",
                new { controller = "News", action = "Create" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_EditNews",
                "admin/news/edit/{id}",
                new { controller = "News", action = "Edit" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_DeleteNews",
                "admin/news/delete/{id}",
                new { controller = "News", action = "Delete" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            #endregion

            #region Routes for Teams
            context.MapRoute(
                "Admin_Teams",
                "admin/teams",
                new { controller = "Teams", action = "Index" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_AddTeam",
                "admin/teams/add",
                new { controller = "Teams", action = "Create" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_EditTeam",
                "admin/teams/edit/{id}",
                new { controller = "Teams", action = "Edit" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_DeleteTeam",
                "admin/teams/delete/{id}",
                new { controller = "Teams", action = "Delete" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            #endregion

            #region Routes for Locations
            context.MapRoute(
                "Admin_Locations",
                "admin/locations",
                new { controller = "Locations", action = "Index" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Admin_AddCountry",
                "admin/locations/add-country",
                new { controller = "Locations", action = "CreateCountry" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_EditCountry",
                "admin/locations/edit-country/{id}",
                new { controller = "Locations", action = "EditCountry" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_DeleteCountry",
                "admin/locations/delete-country/{id}",
                new { controller = "Locations", action = "DeleteCountry" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Admin_AddCity",
                "admin/locations/add-city",
                new { controller = "Locations", action = "CreateCity" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_EditCity",
                "admin/locations/edit-city/{id}",
                new { controller = "Locations", action = "EditCity" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_DeleteCity",
                "admin/locations/delete-city/{id}",
                new { controller = "Locations", action = "DeleteCity" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            #endregion

            #region Routes for Categories
            context.MapRoute(
                "Admin_Categories",
                "admin/categories",
                new { controller = "Categories", action = "Index" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Admin_AddNewsCategory",
                "admin/categories/add-news-category",
                new { controller = "Categories", action = "CreateNewsCategory" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_EditNewsCategory",
                "admin/categories/edit-news-category/{id}",
                new { controller = "Categories", action = "EditNewsCategory" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_DeleteNewsCategory",
                "admin/categories/delete-news-category/{id}",
                new { controller = "Categories", action = "DeleteNewsCategory" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Admin_AddVideoCategory",
                "admin/categories/add-video-category",
                new { controller = "Categories", action = "CreateVideoCategory" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_EditVideoCategory",
                "admin/categories/edit-video-category/{id}",
                new { controller = "Categories", action = "EditVideoCategory" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_DeleteVideoCategory",
                "admin/categories/delete-video-category/{id}",
                new { controller = "Categories", action = "DeleteVideoCategory" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            #endregion

            #region Routes for GameInfo
            context.MapRoute(
                "Admin_GameInfo",
                "admin/game-info",
                new { controller = "GameInfo", action = "Index" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Admin_AddGamePlace",
                "admin/game-info/add-game-place",
                new { controller = "GameInfo", action = "CreateGamePlace" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_EditGamePlace",
                "admin/game-info/edit-game-place/{id}",
                new { controller = "GameInfo", action = "EditGamePlace" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_DeleteGamePlace",
                "admin/game-info/delete-game-place/{id}",
                new { controller = "GameInfo", action = "DeleteGamePlace" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Admin_AddJudge",
                "admin/game-info/add-judge",
                new { controller = "GameInfo", action = "CreateJudge" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_EditJudge",
                "admin/game-info/edit-judge/{id}",
                new { controller = "GameInfo", action = "EditJudge" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_DeleteJudge",
                "admin/game-info/delete-judge/{id}",
                new { controller = "GameInfo", action = "DeleteJudge" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            #endregion

            #region Routes for Achievements
            context.MapRoute(
                "Admin_Achievements",
                "admin/achievements",
                new { controller = "Achievements", action = "Index" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_AddAchievement",
                "admin/achievements/add-achievement",
                new { controller = "Achievements", action = "Create" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_EditAchievement",
                "admin/achievements/edit-achievement/{id}",
                new { controller = "Achievements", action = "Edit" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_DeleteAchievement",
                "admin/achievements/delete-achievement/{id}",
                new { controller = "Achievements", action = "Delete" },
                new { id = @"\d+" },
                new[] { "BaseballApp.Areas.Admin.Controllers" }
            );
            #endregion
        }
    }
}