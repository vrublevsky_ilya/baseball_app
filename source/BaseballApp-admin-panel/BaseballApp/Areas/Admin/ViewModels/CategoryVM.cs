﻿using BaseballApp.Models;
using System.Collections.Generic;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class CategoryVM
    {
        public NewsCategoryVM NewsCategoryVM { get; set; }
        public VideoCategoryVM VideoCategoryVM { get; set; }

        public IEnumerable<Category> NewsCategories { get; set; }
        public IEnumerable<VideoCategory> VideoCategories { get; set; }
    }
}