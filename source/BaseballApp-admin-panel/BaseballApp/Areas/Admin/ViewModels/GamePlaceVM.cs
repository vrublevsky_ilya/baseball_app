﻿using System.ComponentModel.DataAnnotations;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class GamePlaceVM
    {
        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string Name { get; set; }
    }
}