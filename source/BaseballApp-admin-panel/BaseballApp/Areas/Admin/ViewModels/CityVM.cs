﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class CityVM
    {
        [Required(ErrorMessage = "Выберите страну")]
        public int CountryId { get; set; }

        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string NameCity { get; set; }

        public SelectList Countries { get; set; }
    }
}