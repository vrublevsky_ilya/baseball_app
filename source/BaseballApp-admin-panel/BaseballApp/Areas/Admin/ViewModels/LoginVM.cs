﻿using System.ComponentModel.DataAnnotations;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class LoginVM
    {
        [Display(Name = "Логин")]
        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        public string Password { get; set; }

        [Display(Name = "Запомнить")]
        public bool RememberMe { get; set; }
    }
}