﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class CreatePlayerVM
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        public int? CityId { get; set; }
        public int? PositionId { get; set; }
        public int? TeamId { get; set; }

        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string Surname { get; set; }

        public DateTime? Birthday { get; set; }

        [Range(1, 99, ErrorMessage = "Номер может быть до 99")]
        public int? Number { get; set; }

        [Range(1, 250, ErrorMessage = "Рост может быть до 250 см")]
        public int? Height { get; set; }

        [Range(1, 150, ErrorMessage = "Вес может быть до 150 кг")]
        public int? Weight { get; set; }

        public string Attack { get; set; }
        public string Protection { get; set; }
        public bool MainRoster { get; set; }

        public DateTime? DebutMajorLeague { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string PlaceOfStudy { get; set; }

        public string Photo { get; set; }

        public SelectList Cities { get; set; }
        public SelectList Positions { get; set; }
        public SelectList Teams { get; set; }
        public List<SelectListItem> Sides { get; set; } = new List<SelectListItem>(new[]
        {
            new SelectListItem { Text = "Левая", Value = "Л" },
            new SelectListItem { Text = "Правая", Value = "П" },
            new SelectListItem { Text = "Свитч", Value = "С" }
        });
    }
}