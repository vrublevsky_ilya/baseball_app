﻿namespace BaseballApp.Areas.Admin.ViewModels
{
    public class IndexTeamVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Coach { get; set; }
        public int PlayersCount { get; set; }
        public string CityName { get; set; }
        public string CountryAlpha2 { get; set; }
    }
}