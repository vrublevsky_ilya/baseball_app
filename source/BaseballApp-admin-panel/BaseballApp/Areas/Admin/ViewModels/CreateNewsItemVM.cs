﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class CreateNewsItemVM
    {
        public int Id { get; set; }

        [Display(Name = "Категория")]
        public int? CategoryId { get; set; }

        [Display(Name = "Новость")]
        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 30 символов")]
        public string Title { get; set; }

        [Display(Name = "Краткое описание новости")]
        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 150 символов")]
        public string ShortNews { get; set; }

        [Display(Name = "Полное описание новости")]
        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        public string FullNews { get; set; }

        public SelectList Categories { get; set; }
    }
}