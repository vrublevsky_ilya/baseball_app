﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class IndexNewsItemVM
    {
        public int Id { get; set; }

        [Display(Name = "Новость")]
        public string Title { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Дата публикации")]
        public DateTime PubDate { get; set; }

        [Display(Name = "Категория")]
        public string CategoryName { get; set; }
    }
}