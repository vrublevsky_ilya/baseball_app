﻿namespace BaseballApp.Areas.Admin.ViewModels
{
    public class IndexPlayerVM
    {
        public int Id { get; set; }
        public int? Number { get; set; }
        public string Player { get; set; }
        public string Position { get; set; }
        public string Team { get; set; }
        public string CityName { get; set; }
        public string CountryAlpha2 { get; set; }
    }
}