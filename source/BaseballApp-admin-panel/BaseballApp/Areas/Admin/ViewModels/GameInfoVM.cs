﻿using BaseballApp.Models;
using System.Collections.Generic;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class GameInfoVM
    {
        public GamePlaceVM GamePlaceVM { get; set; }
        public JudgeVM JudgeVM { get; set; }

        public IEnumerable<GamePlace> GamePlaces { get; set; }
        public IEnumerable<Judge> Judges { get; set; }
    }
}