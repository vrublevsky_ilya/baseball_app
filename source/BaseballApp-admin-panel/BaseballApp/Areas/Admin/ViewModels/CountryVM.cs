﻿using System.ComponentModel.DataAnnotations;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class CountryVM
    {
        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string NameCountry { get; set; }

        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(2, MinimumLength = 2, ErrorMessage = "Код состоит из двух символов")]
        public string Alpha2 { get; set; }
    }
}