﻿using BaseballApp.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class AchievementVM
    {
        [Required(ErrorMessage = "Выберите команду")]
        public int TeamId { get; set; }

        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string Name { get; set; }

        [Extensions.FileExtensions(Extensions = "png", ErrorMessage = "Выберите изображение в формате PNG")]
        public HttpPostedFileBase Image { get; set; }

        public SelectList Teams { get; set; }

        public IEnumerable<Achievement> Achievements { get; set; }
    }
}