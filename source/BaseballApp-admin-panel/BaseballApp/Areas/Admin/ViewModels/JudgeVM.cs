﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class JudgeVM
    {
        public int? CityId { get; set; }

        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string Surname { get; set; }

        public SelectList Cities { get; set; }
    }
}