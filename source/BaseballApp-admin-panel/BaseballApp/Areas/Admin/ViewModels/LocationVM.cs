﻿using BaseballApp.Models;
using System.Collections.Generic;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class LocationVM
    {
        public CountryVM CountryVM { get; set; }
        public CityVM CityVM { get; set; }

        public IEnumerable<Country> Countries { get; set; }
        public IEnumerable<City> Cities { get; set; }
    }
}