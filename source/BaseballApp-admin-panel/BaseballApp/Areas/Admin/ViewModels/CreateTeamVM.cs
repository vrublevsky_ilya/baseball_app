﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BaseballApp.Areas.Admin.ViewModels
{
    public class CreateTeamVM
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        public int? CityId { get; set; }

        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string FullName { get; set; }

        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть до 50 символов")]
        public string Coach { get; set; }

        public string Logo { get; set; }
        public string BackgroundImage { get; set; }

        public SelectList Cities { get; set; }
    }
}