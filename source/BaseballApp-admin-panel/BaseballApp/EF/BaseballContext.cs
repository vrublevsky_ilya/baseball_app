﻿using BaseballApp.Mapping;
using BaseballApp.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace BaseballApp.EF
{
    public class BaseballContext : IdentityDbContext<User>
    {
        public DbSet<Achievement> Achievements { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<GamePlace> GamePlaces { get; set; }
        public DbSet<Judge> Judges { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<MatchTeams> MatchTeams { get; set; }
        public DbSet<NewsItem> News { get; set; }
        public DbSet<NewsSlide> NewsSlides { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamStats> TeamStats { get; set; }
        public DbSet<Tournament> Tournaments { get; set; }
        public DbSet<TournamentTeams> TournamentTeams { get; set; }
        public DbSet<VideoCategory> VideoCategories { get; set; }
        public DbSet<VideoSlide> VideoSlides { get; set; }
        public DbSet<Weather> Weather { get; set; }
        public DbSet<WidgetNextMatch> WidgetNextMatch { get; set; }

        public BaseballContext(string connectionString) : base(connectionString)
        {
        }

        public BaseballContext()
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AchievementMap());
            modelBuilder.Configurations.Add(new CategoryMap());
            modelBuilder.Configurations.Add(new CityMap());
            modelBuilder.Configurations.Add(new CountryMap());
            modelBuilder.Configurations.Add(new GamePlaceMap());
            modelBuilder.Configurations.Add(new JudgeMap());
            modelBuilder.Configurations.Add(new MatchMap());
            modelBuilder.Configurations.Add(new MatchTeamsMap());
            modelBuilder.Configurations.Add(new NewsItemMap());
            modelBuilder.Configurations.Add(new NewsSlideMap());
            modelBuilder.Configurations.Add(new PartnerMap());
            modelBuilder.Configurations.Add(new PlayerMap());
            modelBuilder.Configurations.Add(new PositionMap());
            modelBuilder.Configurations.Add(new TeamMap());
            modelBuilder.Configurations.Add(new TeamStatsMap());
            modelBuilder.Configurations.Add(new TournamentMap());
            modelBuilder.Configurations.Add(new TournamentTeamsMap());
            modelBuilder.Configurations.Add(new VideoCategoryMap());
            modelBuilder.Configurations.Add(new VideoSlideMap());
            modelBuilder.Configurations.Add(new WeatherMap());
            modelBuilder.Configurations.Add(new WidgetNextMatchMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}