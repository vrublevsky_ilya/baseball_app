﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class MatchTeamsRepository : IRepository<MatchTeams>
    {
        private BaseballContext db;

        public MatchTeamsRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<MatchTeams> GetAll()
        {
            return db.MatchTeams.ToList();
        }

        public MatchTeams Get(int id)
        {
            return db.MatchTeams.Find(id);
        }

        public void Create(MatchTeams matchTeams)
        {
            db.MatchTeams.Add(matchTeams);
        }

        public void Update(MatchTeams matchTeams)
        {
            db.Entry(matchTeams).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            MatchTeams matchTeams = db.MatchTeams.Find(id);
            if (matchTeams != null)
                db.MatchTeams.Remove(matchTeams);
        }
    }
}