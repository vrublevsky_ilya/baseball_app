﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class MatchRepository : IRepository<Match>
    {
        private BaseballContext db;

        public MatchRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<Match> GetAll()
        {
            return db.Matches.ToList();
        }

        public IEnumerable<Match> GetByCount(int takeCount)
        {
            return db.Matches
                .Where(p => p.TeamWinner != null)
                .OrderByDescending(ks => ks.StartDate)
                .Take(takeCount)
                .ToList();
        }

        public IEnumerable<Match> GetByTeam(int teamId, int takeCount)
        {
            return db.Matches
                .Where(p => p.TeamWinner != null && p.MatchTeams.Any(t => t.TeamId == teamId))
                .OrderByDescending(ks => ks.StartDate)
                .Take(takeCount)
                .ToList();
        }

        public IEnumerable<Match> UpcomingGetByTournament(int tournamentId, int takeCount)
        {
            return db.Matches
                .Where(p => p.TournamentId == tournamentId && p.StartDate > DateTime.Now)
                .OrderBy(ks => ks.StartDate)
                .Take(takeCount)
                .ToList();
        }

        public IEnumerable<Match> LatestGetByTournament(int tournamentId, int takeCount)
        {
            return db.Matches
                .Where(p => p.TournamentId == tournamentId && p.TeamWinner != null)
                .OrderByDescending(ks => ks.StartDate)
                .Take(takeCount)
                .ToList();
        }

        public Match Get(int id)
        {
            return db.Matches.Find(id);
        }

        public void Create(Match match)
        {
            db.Matches.Add(match);
        }

        public void Update(Match match)
        {
            db.Entry(match).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Match match = db.Matches.Find(id);
            if (match != null)
                db.Matches.Remove(match);
        }
    }
}