﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class PartnerRepository : IRepository<Partner>
    {
        private BaseballContext db;

        public PartnerRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<Partner> GetAll()
        {
            return db.Partners.ToList();
        }

        public Partner Get(int id)
        {
            return db.Partners.Find(id);
        }

        public void Create(Partner partner)
        {
            db.Partners.Add(partner);
        }

        public void Update(Partner partner)
        {
            db.Entry(partner).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Partner partner = db.Partners.Find(id);
            if (partner != null)
                db.Partners.Remove(partner);
        }
    }
}