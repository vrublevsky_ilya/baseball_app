﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class CityRepository : IRepository<City>
    {
        private BaseballContext db;

        public CityRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<City> GetAll()
        {
            return db.Cities
                .OrderBy(ks => ks.Name)
                .ToList();
        }

        public City Get(int id)
        {
            return db.Cities.Find(id);
        }

        public void Create(City city)
        {
            db.Cities.Add(city);
        }

        public void Update(City city)
        {
            db.Entry(city).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            City city = db.Cities.Find(id);
            if (city != null)
                db.Cities.Remove(city);
        }
    }
}