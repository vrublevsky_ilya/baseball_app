﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class VideoCategoryRepository : IRepository<VideoCategory>
    {
        private BaseballContext db;

        public VideoCategoryRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<VideoCategory> GetAll()
        {
            return db.VideoCategories.ToList();
        }

        public VideoCategory Get(int id)
        {
            return db.VideoCategories.Find(id);
        }

        public void Create(VideoCategory videoCategory)
        {
            db.VideoCategories.Add(videoCategory);
        }

        public void Update(VideoCategory videoCategory)
        {
            db.Entry(videoCategory).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            VideoCategory videoCategory = db.VideoCategories.Find(id);
            if (videoCategory != null)
                db.VideoCategories.Remove(videoCategory);
        }
    }
}