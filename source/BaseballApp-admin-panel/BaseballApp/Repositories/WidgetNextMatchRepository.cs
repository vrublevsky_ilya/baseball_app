﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class WidgetNextMatchRepository : IRepository<WidgetNextMatch>
    {
        private BaseballContext db;

        public WidgetNextMatchRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<WidgetNextMatch> GetAll()
        {
            return db.WidgetNextMatch.ToList();
        }

        public WidgetNextMatch Get(int id)
        {
            return db.WidgetNextMatch.Find(id);
        }

        public void Create(WidgetNextMatch widgetNextMatch)
        {
            db.WidgetNextMatch.Add(widgetNextMatch);
        }

        public void Update(WidgetNextMatch widgetNextMatch)
        {
            db.Entry(widgetNextMatch).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            WidgetNextMatch widgetNextMatch = db.WidgetNextMatch.Find(id);
            if (widgetNextMatch != null)
                db.WidgetNextMatch.Remove(widgetNextMatch);
        }
    }
}