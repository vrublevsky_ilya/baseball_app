﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private BaseballContext db;

        public TeamRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<Team> GetAll()
        {
            return db.Teams.ToList();
        }

        public Team Get(int id)
        {
            return db.Teams.Find(id);
        }

        public void Create(Team team)
        {
            db.Teams.Add(team);
        }

        public void Update(Team team)
        {
            db.Entry(team).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Team team = db.Teams.Find(id);
            if (team != null)
                db.Teams.Remove(team);
        }
    }
}