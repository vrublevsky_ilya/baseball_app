﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class WeatherRepository : IRepository<Weather>
    {
        private BaseballContext db;

        public WeatherRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<Weather> GetAll()
        {
            return db.Weather.ToList();
        }

        public Weather Get(int id)
        {
            return db.Weather.Find(id);
        }

        public void Create(Weather weather)
        {
            db.Weather.Add(weather);
        }

        public void Update(Weather weather)
        {
            db.Entry(weather).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Weather weather = db.Weather.Find(id);
            if (weather != null)
                db.Weather.Remove(weather);
        }
    }
}