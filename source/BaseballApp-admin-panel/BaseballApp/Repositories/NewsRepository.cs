﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class NewsRepository : IRepository<NewsItem>
    {
        private BaseballContext db;

        public NewsRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<NewsItem> GetAll()
        {
            return db.News.ToList();
        }

        public NewsItem[] GetByCount(int takeCount)
        {
            return db.News
                .OrderByDescending(ks => ks.PubDate)
                .Take(takeCount)
                .ToArray();
        }

        public NewsItem Get(int id)
        {
            return db.News.Find(id);
        }

        public void Create(NewsItem newsItem)
        {
            var item = db.News.Add(newsItem);
        }

        public void Update(NewsItem newsItem)
        {
            db.Entry(newsItem).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            NewsItem newsItem = db.News.Find(id);
            if (newsItem != null)
                db.News.Remove(newsItem);
        }
    }
}