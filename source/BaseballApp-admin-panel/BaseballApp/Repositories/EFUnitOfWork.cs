﻿using BaseballApp.EF;
using BaseballApp.Identity;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;

namespace BaseballApp.Repositories
{
    public class EFUnitOfWork : IUnitOfWork, IIdentityUnitOfWork
    {
        private readonly BaseballContext _db;

        private AppUserManager _appUserManager;
        private AppSignInManager _appSignInManager;

        private AchievementRepository _achievementRepository;
        private CategoryRepository _categoryRepository;
        private CityRepository _cityRepository;
        private CountryRepository _countryRepository;
        private GamePlaceRepository _gamePlaceRepository;
        private JudgeRepository _judgeRepository;
        public MatchTeamsRepository MatchTeamsRepository;
        private NewsRepository _newsRepository;
        private NewsSlideRepository _newsSlideRepository;
        private PartnerRepository _partnerRepository;
        private PlayerRepository _playerRepository;
        private PositionRepository _positionRepository;
        private TeamRepository _teamRepository;
        private TeamStatsRepository _teamStatsRepository;
        private TournamentRepository _tournamentRepository;
        private TournamentTeamsRepository _tournamentTeamsRepository;
        private VideoCategoryRepository _videoCategoryRepository;
        private VideoSlideRepository _videoSlideRepository;
        private WeatherRepository _weatherRepository;
        private WidgetNextMatchRepository _widgetNextMatchRepository;

        private AchievementRepository _achievementRepositoryExtended;
        private NewsRepository _newsRepositoryExtended;
        private TeamStatsRepository _teamStatsRepositoryExtended;
        private MatchRepository _matchRepositoryExtended;
        private PlayerRepository _playerRepositoryExtended;
        private TournamentTeamsRepository _tournamentTeamsRepositoryExtended;

        public EFUnitOfWork()
        {
            _db = new BaseballContext("DefaultConnection");
        }

        public IAuthenticationManager AppAuthManager { get; set; }

        public AppUserManager AppUserManager => _appUserManager ?? (_appUserManager = new AppUserManager(new UserStore<User>(_db)));
        public AppSignInManager AppSignInManager => _appSignInManager ?? (_appSignInManager = new AppSignInManager(AppUserManager, AppAuthManager));
        public IRepository<Achievement> Achievements => _achievementRepository ?? (_achievementRepository = new AchievementRepository(_db));
        public IRepository<Category> Categories => _categoryRepository ?? (_categoryRepository = new CategoryRepository(_db));
        public IRepository<City> Cities => _cityRepository ?? (_cityRepository = new CityRepository(_db));
        public IRepository<Country> Countries => _countryRepository ?? (_countryRepository = new CountryRepository(_db));
        public IRepository<GamePlace> GamePlaces => _gamePlaceRepository ?? (_gamePlaceRepository = new GamePlaceRepository(_db));
        public IRepository<Judge> Judges => _judgeRepository ?? (_judgeRepository = new JudgeRepository(_db));
        public IRepository<Match> Matches => MatchRepository ?? (MatchRepository = new MatchRepository(_db));
        public IRepository<MatchTeams> MatchTeams => MatchTeamsRepository ?? (MatchTeamsRepository = new MatchTeamsRepository(_db));
        public IRepository<NewsItem> News => _newsRepository ?? (_newsRepository = new NewsRepository(_db));
        public IRepository<NewsSlide> NewsSlides => _newsSlideRepository ?? (_newsSlideRepository = new NewsSlideRepository(_db));
        public IRepository<Partner> Partners => _partnerRepository ?? (_partnerRepository = new PartnerRepository(_db));
        public IRepository<Player> Players => _playerRepository ?? (_playerRepository = new PlayerRepository(_db));
        public IRepository<Position> Positions => _positionRepository ?? (_positionRepository = new PositionRepository(_db));
        public IRepository<Team> Teams => _teamRepository ?? (_teamRepository = new TeamRepository(_db));
        public IRepository<TeamStats> TeamStats => _teamStatsRepository ?? (_teamStatsRepository = new TeamStatsRepository(_db));
        public IRepository<Tournament> Tournaments => _tournamentRepository ?? (_tournamentRepository = new TournamentRepository(_db));
        public IRepository<TournamentTeams> TournamentTeams => _tournamentTeamsRepository ?? (_tournamentTeamsRepository = new TournamentTeamsRepository(_db));
        public IRepository<VideoCategory> VideoCategories => _videoCategoryRepository ?? (_videoCategoryRepository = new VideoCategoryRepository(_db));
        public IRepository<VideoSlide> VideoSlides => _videoSlideRepository ?? (_videoSlideRepository = new VideoSlideRepository(_db));
        public IRepository<Weather> Weather => _weatherRepository ?? (_weatherRepository = new WeatherRepository(_db));
        public IRepository<WidgetNextMatch> WidgetNextMatch => _widgetNextMatchRepository ?? (_widgetNextMatchRepository = new WidgetNextMatchRepository(_db));
        public AchievementRepository AchievementsExtended => _achievementRepositoryExtended ?? (_achievementRepositoryExtended = new AchievementRepository(_db));
        public NewsRepository NewsExtended => _newsRepositoryExtended ?? (_newsRepositoryExtended = new NewsRepository(_db));
        public TeamStatsRepository TeamStatsExtended => _teamStatsRepositoryExtended ?? (_teamStatsRepositoryExtended = new TeamStatsRepository(_db));
        public MatchRepository MatchesExtended => _matchRepositoryExtended ?? (_matchRepositoryExtended = new MatchRepository(_db));
        public PlayerRepository PlayersExtended => _playerRepositoryExtended ?? (_playerRepositoryExtended = new PlayerRepository(_db));
        public TournamentTeamsRepository TournamentTeamsExtended => _tournamentTeamsRepositoryExtended ?? (_tournamentTeamsRepositoryExtended = new TournamentTeamsRepository(_db));
        public MatchRepository MatchRepository { get; set; }

        public void Save()
        {
            try
            {
                _db.SaveChanges();
            }
            catch (Exception e)
            {
                throw new AggregateException(e);
            }
        }

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _appUserManager?.Dispose();
                    _appSignInManager?.Dispose();

                    _db.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}