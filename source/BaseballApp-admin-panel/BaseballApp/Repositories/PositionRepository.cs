﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class PositionRepository : IRepository<Position>
    {
        private BaseballContext db;

        public PositionRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<Position> GetAll()
        {
            return db.Positions.ToList();
        }

        public Position Get(int id)
        {
            return db.Positions.Find(id);
        }

        public void Create(Position position)
        {
            db.Positions.Add(position);
        }

        public void Update(Position position)
        {
            db.Entry(position).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Position position = db.Positions.Find(id);
            if (position != null)
                db.Positions.Remove(position);
        }
    }
}