﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class TeamStatsRepository : IRepository<TeamStats>
    {
        private BaseballContext db;

        public TeamStatsRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<TeamStats> GetAll()
        {
            return db.TeamStats.ToList();
        }

        public IEnumerable<TeamStats> GetByCount(int takeCount)
        {
            return db.TeamStats
                .Where(p => p.Year == DateTime.Now.Year)
                .OrderByDescending(ks => ks.Victories)
                .Take(takeCount)
                .ToList();
        }

        public IEnumerable<TeamStats> GetByTeam(int teamId)
        {
            return db.TeamStats
                .Where(p => p.TeamId == teamId)
                .OrderBy(ks => ks.Year)
                .ToList();
        }

        public TeamStats Get(int id)
        {
            return db.TeamStats.Find(id);
        }

        public void Create(TeamStats teamStats)
        {
            db.TeamStats.Add(teamStats);
        }

        public void Update(TeamStats teamStats)
        {
            db.Entry(teamStats).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            TeamStats teamStats = db.TeamStats.Find(id);
            if (teamStats != null)
                db.TeamStats.Remove(teamStats);
        }
    }
}