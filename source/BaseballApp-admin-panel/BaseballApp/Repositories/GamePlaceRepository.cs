﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class GamePlaceRepository : IRepository<GamePlace>
    {
        private BaseballContext db;

        public GamePlaceRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<GamePlace> GetAll()
        {
            return db.GamePlaces.ToList();
        }

        public GamePlace Get(int id)
        {
            return db.GamePlaces.Find(id);
        }

        public void Create(GamePlace gamePlace)
        {
            db.GamePlaces.Add(gamePlace);
        }

        public void Update(GamePlace gamePlace)
        {
            db.Entry(gamePlace).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            GamePlace gamePlace = db.GamePlaces.Find(id);
            if (gamePlace != null)
                db.GamePlaces.Remove(gamePlace);
        }
    }
}