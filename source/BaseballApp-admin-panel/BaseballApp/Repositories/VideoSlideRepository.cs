﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class VideoSlideRepository : IRepository<VideoSlide>
    {
        private BaseballContext db;

        public VideoSlideRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<VideoSlide> GetAll()
        {
            return db.VideoSlides.ToList();
        }

        public VideoSlide Get(int id)
        {
            return db.VideoSlides.Find(id);
        }

        public void Create(VideoSlide videoSlide)
        {
            db.VideoSlides.Add(videoSlide);
        }

        public void Update(VideoSlide videoSlide)
        {
            db.Entry(videoSlide).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            VideoSlide videoSlide = db.VideoSlides.Find(id);
            if (videoSlide != null)
                db.VideoSlides.Remove(videoSlide);
        }
    }
}