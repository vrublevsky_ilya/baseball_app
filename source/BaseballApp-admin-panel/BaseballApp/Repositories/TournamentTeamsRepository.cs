﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class TournamentTeamsRepository : IRepository<TournamentTeams>
    {
        private BaseballContext db;

        public TournamentTeamsRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<TournamentTeams> GetAll()
        {
            return db.TournamentTeams.ToList();
        }

        public IEnumerable<TournamentTeams> GetTournamentsInfoByTeam(int teamId)
        {
            return db.TournamentTeams
                .Where(p => p.TeamId == teamId && p.Tournament.EndDate < DateTime.Now)
                .OrderByDescending(ks => ks.Tournament.StartDate)
                .ToList();
        }

        public TournamentTeams Get(int id)
        {
            return db.TournamentTeams.Find(id);
        }

        public void Create(TournamentTeams tournamentTeams)
        {
            db.TournamentTeams.Add(tournamentTeams);
        }

        public void Update(TournamentTeams tournamentTeams)
        {
            db.Entry(tournamentTeams).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            TournamentTeams tournamentTeams = db.TournamentTeams.Find(id);
            if (tournamentTeams != null)
                db.TournamentTeams.Remove(tournamentTeams);
        }
    }
}