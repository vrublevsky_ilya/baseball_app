﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class PlayerRepository : IRepository<Player>
    {
        private BaseballContext db;

        public PlayerRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<Player> GetAll()
        {
            return db.Players.ToList();
        }

        public IEnumerable<Player> GetByTeam(int teamId)
        {
            return db.Players
                .Where(p => p.TeamId == teamId)
                .OrderBy(ks => ks.Number)
                .ToList();
        }

        public IEnumerable<Player> GetMainRosterByTeam(int teamId)
        {
            return db.Players
                .Where(p => p.TeamId == teamId && p.MainRoster == true)
                .OrderBy(ks => ks.Number)
                .ToList();
        }

        public Player Get(int id)
        {
            return db.Players.Find(id);
        }

        public void Create(Player player)
        {
            db.Players.Add(player);
        }

        public void Update(Player player)
        {
            db.Entry(player).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Player player = db.Players.Find(id);
            if (player != null)
                db.Players.Remove(player);
        }
    }
}