﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class CountryRepository : IRepository<Country>
    {
        private BaseballContext db;

        public CountryRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<Country> GetAll()
        {
            return db.Countries
                .OrderBy(ks => ks.Name)
                .ToList();
        }

        public Country Get(int id)
        {
            return db.Countries.Find(id);
        }

        public void Create(Country country)
        {
            db.Countries.Add(country);
        }

        public void Update(Country country)
        {
            db.Entry(country).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Country country = db.Countries.Find(id);
            if (country != null)
                db.Countries.Remove(country);
        }
    }
}