﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class AchievementRepository : IRepository<Achievement>
    {
        private BaseballContext db;

        public AchievementRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<Achievement> GetAll()
        {
            return db.Achievements.ToList();
        }

        public IEnumerable<Achievement> GetByTeam(int teamId)
        {
            return db.Achievements
                .Where(p => p.TeamId == teamId)
                .OrderByDescending(ks => ks.Id)
                .ToList();
        }

        public Achievement Get(int id)
        {
            return db.Achievements.Find(id);
        }

        public void Create(Achievement achievement)
        {
            db.Achievements.Add(achievement);
        }

        public void Update(Achievement achievement)
        {
            db.Entry(achievement).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Achievement achievement = db.Achievements.Find(id);
            if (achievement != null)
                db.Achievements.Remove(achievement);
        }
    }
}