﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class NewsSlideRepository : IRepository<NewsSlide>
    {
        private BaseballContext db;

        public NewsSlideRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<NewsSlide> GetAll()
        {
            return db.NewsSlides.ToList();
        }

        public NewsSlide Get(int id)
        {
            return db.NewsSlides.Find(id);
        }

        public void Create(NewsSlide newsSlide)
        {
            db.NewsSlides.Add(newsSlide);
        }

        public void Update(NewsSlide newsSlide)
        {
            db.Entry(newsSlide).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            NewsSlide newsSlide = db.NewsSlides.Find(id);
            if (newsSlide != null)
                db.NewsSlides.Remove(newsSlide);
        }
    }
}