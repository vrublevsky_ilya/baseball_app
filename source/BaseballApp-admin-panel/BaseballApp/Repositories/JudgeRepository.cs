﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class JudgeRepository : IRepository<Judge>
    {
        private BaseballContext db;

        public JudgeRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<Judge> GetAll()
        {
            return db.Judges.ToList();
        }

        public Judge Get(int id)
        {
            return db.Judges.Find(id);
        }

        public void Create(Judge judge)
        {
            db.Judges.Add(judge);
        }

        public void Update(Judge judge)
        {
            db.Entry(judge).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Judge judge = db.Judges.Find(id);
            if (judge != null)
                db.Judges.Remove(judge);
        }
    }
}