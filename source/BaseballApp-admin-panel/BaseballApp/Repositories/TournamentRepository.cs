﻿using BaseballApp.EF;
using BaseballApp.Interfaces;
using BaseballApp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BaseballApp.Repositories
{
    public class TournamentRepository : IRepository<Tournament>
    {
        private BaseballContext db;

        public TournamentRepository(BaseballContext context)
        {
            db = context;
        }

        public IEnumerable<Tournament> GetAll()
        {
            return db.Tournaments.ToList();
        }

        public Tournament Get(int id)
        {
            return db.Tournaments.Find(id);
        }

        public void Create(Tournament tournament)
        {
            db.Tournaments.Add(tournament);
        }

        public void Update(Tournament tournament)
        {
            db.Entry(tournament).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Tournament tournament = db.Tournaments.Find(id);
            if (tournament != null)
                db.Tournaments.Remove(tournament);
        }
    }
}