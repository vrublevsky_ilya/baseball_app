﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace BaseballApp.Extensions
{
    public class FileExtensionsAttribute : DataTypeAttribute, IClientValidatable
    {
        private readonly System.ComponentModel.DataAnnotations.FileExtensionsAttribute innerAttribute = new System.ComponentModel.DataAnnotations.FileExtensionsAttribute();

        public FileExtensionsAttribute() : base(DataType.Upload)
        {            
        }

        public string Extensions
        {
            get { return innerAttribute.Extensions; }
            set { innerAttribute.Extensions = value; }
        }

        public new string ErrorMessage
        {
            get { return innerAttribute.ErrorMessage; }
            set { innerAttribute.ErrorMessage = value; }
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ValidationType = "extension",
                ErrorMessage = ErrorMessage
            };
            rule.ValidationParameters["extension"] = innerAttribute.Extensions;

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file != null)
            {
                return innerAttribute.IsValid(file.FileName);
            }

            return innerAttribute.IsValid(value);
        }
    }
}