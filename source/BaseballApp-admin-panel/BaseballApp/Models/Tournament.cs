﻿using System;
using System.Collections.Generic;

namespace BaseballApp.Models
{
    public class Tournament
    {
        public int Id { get; set; }
        public int? JudgeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Logo { get; set; }

        public virtual Judge Judge { get; set; }

        public virtual ICollection<Match> Matches { get; set; }
        public virtual ICollection<TournamentTeams> TournamentTeams { get; set; }

        public Tournament()
        {
            Matches = new List<Match>();
            TournamentTeams = new List<TournamentTeams>();
        }
    }
}