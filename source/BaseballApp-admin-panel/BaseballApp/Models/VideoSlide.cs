﻿namespace BaseballApp.Models
{
    public class VideoSlide
    {
        public int Id { get; set; }
        public int? VideoCategoryId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }

        public virtual VideoCategory VideoCategory { get; set; }
    }
}