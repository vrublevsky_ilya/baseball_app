﻿using System;

namespace BaseballApp.Models
{
    public class NewsItem
    {
        public int Id { get; set; }
        public int? CategoryId { get; set; }
        public string Title { get; set; }
        public string ShortNews { get; set; }
        public string FullNews { get; set; }
        public DateTime PubDate { get; set; }
        public string Image { get; set; }

        public virtual Category Category { get; set; }
        public virtual NewsSlide NewsSlide { get; set; }
    }
}