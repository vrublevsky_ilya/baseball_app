﻿namespace BaseballApp.Models
{
    public class Achievement
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }

        public virtual Team Team { get; set; }
    }
}