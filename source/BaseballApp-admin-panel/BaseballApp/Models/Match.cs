﻿using System;
using System.Collections.Generic;

namespace BaseballApp.Models
{
    public class Match
    {
        public int Id { get; set; }
        public int? TournamentId { get; set; }
        public int? GamePlaceId { get; set; }
        public int? WeatherId { get; set; }
        public int? JudgeId { get; set; }
        public int? TeamWinnerId { get; set; }
        public DateTime StartDate { get; set; }

        public virtual Tournament Tournament { get; set; }
        public virtual GamePlace GamePlace { get; set; }
        public virtual Weather Weather { get; set; }
        public virtual Judge Judge { get; set; }
        public virtual Team TeamWinner { get; set; }
        public virtual WidgetNextMatch WidgetNextMatch { get; set; }

        public virtual ICollection<MatchTeams> MatchTeams { get; set; }

        public Match()
        {
            MatchTeams = new List<MatchTeams>();
        }
    }
}