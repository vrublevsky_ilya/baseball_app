﻿using System.Collections.Generic;

namespace BaseballApp.Models
{
    public class VideoCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<VideoSlide> VideoSlides { get; set; }

        public VideoCategory()
        {
            VideoSlides = new List<VideoSlide>();
        }
    }
}