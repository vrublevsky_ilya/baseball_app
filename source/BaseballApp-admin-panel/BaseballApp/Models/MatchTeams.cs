﻿namespace BaseballApp.Models
{
    public class MatchTeams
    {
        public int MatchId { get; set; }
        public int TeamId { get; set; }
        public int? Inning1 { get; set; }
        public int? Inning2 { get; set; }
        public int? Inning3 { get; set; }
        public int? Inning4 { get; set; }
        public int? Inning5 { get; set; }
        public int? Inning6 { get; set; }
        public int? Inning7 { get; set; }
        public int? Inning8 { get; set; }
        public int? Inning9 { get; set; }
        public int? R { get; set; }
        public int? H { get; set; }
        public int? E { get; set; }

        public virtual Match Match { get; set; }
        public virtual Team Team { get; set; }
    }
}