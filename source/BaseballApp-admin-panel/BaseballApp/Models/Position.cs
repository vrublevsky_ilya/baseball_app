﻿using System.Collections.Generic;

namespace BaseballApp.Models
{
    public class Position
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }

        public virtual ICollection<Player> Players { get; set; }

        public Position()
        {
            Players = new List<Player>();
        }
    }
}