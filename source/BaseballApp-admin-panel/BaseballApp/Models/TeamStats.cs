﻿namespace BaseballApp.Models
{
    public class TeamStats
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public int Year { get; set; }
        public int Victories { get; set; }
        public int Defeats { get; set; }

        public virtual Team Team { get; set; }
    }
}