﻿using System.Collections.Generic;

namespace BaseballApp.Models
{
    public class City
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }

        public virtual Country Country { get; set; }

        public virtual ICollection<Judge> Judges { get; set; }
        public virtual ICollection<Player> Players { get; set; }
        public virtual ICollection<Team> Teams { get; set; }

        public City()
        {
            Judges = new List<Judge>();
            Players = new List<Player>();
            Teams = new List<Team>();
        }
    }
}