﻿namespace BaseballApp.Models
{
    public class TournamentTeams
    {
        public int TournamentId { get; set; }
        public int TeamId { get; set; }
        public int? Place { get; set; }
        public int Victories { get; set; }
        public int Defeats { get; set; }

        public virtual Tournament Tournament { get; set; }
        public virtual Team Team { get; set; }
    }
}