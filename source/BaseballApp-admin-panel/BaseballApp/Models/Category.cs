﻿using System.Collections.Generic;

namespace BaseballApp.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<NewsItem> NewsItems { get; set; }

        public Category()
        {
            NewsItems = new List<NewsItem>();
        }
    }
}