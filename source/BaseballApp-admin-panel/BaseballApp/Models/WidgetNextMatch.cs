﻿namespace BaseballApp.Models
{
    public class WidgetNextMatch
    {
        public int Id { get; set; }

        public virtual Match Match { get; set; }
    }
}