﻿namespace BaseballApp.Models
{
    public class NewsSlide
    {
        public int Id { get; set; }

        public virtual NewsItem NewsItem { get; set; }
    }
}