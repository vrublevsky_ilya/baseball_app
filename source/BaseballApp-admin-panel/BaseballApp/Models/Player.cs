﻿using System;

namespace BaseballApp.Models
{
    public class Player
    {
        public int Id { get; set; }
        public int? CityId { get; set; }
        public int? PositionId { get; set; }
        public int? TeamId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime? Birthday { get; set; }
        public int? Number { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        public string Attack { get; set; }
        public string Protection { get; set; }
        public bool MainRoster { get; set; }
        public string Photo { get; set; }
        public DateTime? DebutMajorLeague { get; set; }
        public string PlaceOfStudy { get; set; }

        public virtual City City { get; set; }
        public virtual Position Position { get; set; }
        public virtual Team Team { get; set; }
    }
}