﻿using System.Collections.Generic;

namespace BaseballApp.Models
{
    public class Judge
    {
        public int Id { get; set; }
        public int? CityId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public virtual City City { get; set; }

        public virtual ICollection<Tournament> Tournaments { get; set; }
        public virtual ICollection<Match> Matches { get; set; }

        public Judge()
        {
            Tournaments = new List<Tournament>();
            Matches = new List<Match>();
        }
    }
}