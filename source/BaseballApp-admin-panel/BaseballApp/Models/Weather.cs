﻿using System.Collections.Generic;

namespace BaseballApp.Models
{
    public class Weather
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Match> Matches { get; set; }

        public Weather()
        {
            Matches = new List<Match>();
        }
    }
}