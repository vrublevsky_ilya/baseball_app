﻿using System.Collections.Generic;

namespace BaseballApp.Models
{
    public class Team
    {
        public int Id { get; set; }
        public int? CityId { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Coach { get; set; }
        public string Logo { get; set; }
        public string BackgroundImage { get; set; }

        public virtual City City { get; set; }

        public virtual ICollection<Player> Players { get; set; }
        public virtual ICollection<Match> Matches { get; set; }
        public virtual ICollection<MatchTeams> MatchTeams { get; set; }
        public virtual ICollection<TeamStats> TeamStats { get; set; }
        public virtual ICollection<Achievement> Achievements { get; set; }
        public virtual ICollection<TournamentTeams> TournamentTeams { get; set; }

        public Team()
        {
            Players = new List<Player>();
            Matches = new List<Match>();
            MatchTeams = new List<MatchTeams>();
            TeamStats = new List<TeamStats>();
            Achievements = new List<Achievement>();
            TournamentTeams = new List<TournamentTeams>();
        }
    }
}