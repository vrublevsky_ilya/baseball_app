﻿using System.Collections.Generic;

namespace BaseballApp.Models
{
    public class GamePlace
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Match> Matches { get; set; }

        public GamePlace()
        {
            Matches = new List<Match>();
        }
    }
}